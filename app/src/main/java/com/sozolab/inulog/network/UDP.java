package com.sozolab.inulog.network;

import android.util.Log;

import com.sozolab.inulog.storage.Preference;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;


public class UDP {

    public static boolean doSend = false;
    public static String address;
    public static int port = 6666;

    private static Preference prefs;


    /**
     * Send string
     *
     * @param message     string to send
     * @throws IOException
     */
    //public static void send(String message, String address, int port) throws IOException {
    public static void send(String message) throws IOException {
        if (!prefs.getBoolean("udp_on")) return;
        address = prefs.get("udp_address");
        if (address==null||address == "") return;

        try (DatagramSocket clientSocket = new DatagramSocket()) {
            InetAddress IPAddress = InetAddress.getByName(address);
            byte[] sendData = message.getBytes();//convertToBytes(message);
            //Log.v("UDP", ""+doSend);
            Log.v("UDP", address + ":"+port + ";" + message);

            DatagramPacket sendPacket = new DatagramPacket(sendData, sendData.length, IPAddress, port);
            clientSocket.send(sendPacket);
        }
    }


}