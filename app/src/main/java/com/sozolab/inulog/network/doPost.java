package com.sozolab.inulog.network;

import android.content.Context;
import android.net.ConnectivityManager;
import android.os.StrictMode;
import android.util.Log;

import com.sozolab.inulog.MyApp;
import com.sozolab.inulog.activity.MainActivity;
import com.sozolab.inulog.service.LogService;
import com.sozolab.inulog.storage.Preference;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.ConnectionPoolTimeoutException;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;


public class DoPost {

	private static Preference prefs;

	//clientを
	//public static String def_upload_url = "http://eneact-test.apps.salt-dev.space";
	public static String def_upload_url = "https://test.eneact.sozolab.jp";
	public static String def_login_url = "https://test.eneact.sozolab.jp";


	public static HttpClient client = HttpClientHelper.getHttpClient();
	public static String info;
	public static boolean loggedIn = false;

	private static final String TAG="DoPost";

	private static Context context;

	public static void init(Context context){
		DoPost.context = context;


		/*
		StrictMode.setThreadPolicy(new StrictMode.ThreadPolicy.Builder()
				//.detectDiskReads().detectDiskWrites()
				.detectNetwork().penaltyLog().build());
				*/
		prefs.setIfEmpty("upload_url", def_upload_url); // set default if null
		prefs.setIfEmpty("login_url", def_login_url); // set default if null
	}


	//login
	public static boolean login(String user,String pass) {
		if (!checkNetworkAvailable()){
			Log.i("Login", "Login abort. Network unavailable");
			return false;
		}

		Log.i(TAG, "Try login as user:"+user);

		String url = prefs.get("login_url");
		org.apache.http.client.methods.HttpPost mPost = new org.apache.http.client.methods.HttpPost(url + "/login");
		//HttpPost mPost = new HttpPost("http://eneact_test.sozolab.jp"+"/login");
		Log.i(TAG, "login_url:" + url + "/login");

		List<BasicNameValuePair> pairs = new ArrayList<BasicNameValuePair>();
		pairs.add(new BasicNameValuePair("login", user));
		pairs.add(new BasicNameValuePair("password", pass));
		try {
			mPost.setEntity(new UrlEncodedFormEntity(pairs, HTTP.UTF_8));
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			//postの方法で
			HttpResponse responsePost = client.execute(mPost);
			int resPost = responsePost.getStatusLine().getStatusCode();
			try {
				if (resPost == 200) {
					HttpEntity entity = responsePost.getEntity();
					loggedIn = true;
					Log.i(TAG, "Logged in with response:" + resPost);
					return true;
				} else {
					loggedIn = false;
					Log.i(TAG, "Server returned illegal response:" + resPost);
					return false;
				}
			} catch (Exception e) {
				System.out.println("message1:" + e);
				return false;
			}
		} catch (ClientProtocolException e) {
			// TODO Auto-generated catch block
			//e.printStackTrace();
			System.out.println("message2:" + e);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			//e.printStackTrace();
			System.out.println("message3:" + e);
		}
		return false;
	}

	//dopost
	public static boolean doPost(String url,String filenameKey,String filename,
		String typeKey, String type, String versionkey, String version,String postKey,String data) {
		Log.v(TAG, filename);

		try {

			if (!checkNetworkAvailable()) {
				Log.d(TAG, "Network unavailable.");
				return false;
			}
			if (!loggedIn) {
				Log.d(TAG, "Not logged in.");
				return false;
			}

			HttpParams httpParams = client.getParams();
			//接続確立のタイムアウトを設定（単位：ms）
			HttpConnectionParams.setConnectionTimeout(httpParams, 30 * 1000);
			//接続後のタイムアウトを設定（単位：ms）

			HttpConnectionParams.setSoTimeout(httpParams, 60 * 1000);

			org.apache.http.client.methods.HttpPost method = new org.apache.http.client.methods.HttpPost(url);
			// リクエストパラメータの設定
			List<NameValuePair> params = new ArrayList<NameValuePair>();
			params.add(new BasicNameValuePair(filenameKey, filename));
			params.add(new BasicNameValuePair(typeKey, type));
			params.add(new BasicNameValuePair(versionkey, version));
			params.add(new BasicNameValuePair(postKey, data));
			method.setEntity(new UrlEncodedFormEntity(params, "utf-8"));

			HttpResponse response = client.execute(method);
			int status = response.getStatusLine().getStatusCode();
			if (status == 200) {
				//successを取る
				HttpEntity entity = response.getEntity();
				if (entity != null) {
					info = EntityUtils.toString(entity);
					Log.d(TAG, "doPost success with response 200.");
				}
			} else {
				Log.d(TAG, "Server response is not 200.");
				//throw new Exception("Server response is not 200.");
				//結構このエラーが出るのは?network/loginのいずれかが失敗？
			}

			//Log.v("Inulog", "DoPost " + filename + " finished");
			//Log.v("Status", url);
			return true;
		} catch (UnknownHostException e) {
			Log.d(TAG, "UnknownHostException ignored.");
		} catch (ConnectionPoolTimeoutException e) {
			Log.d(TAG, "ConnectionPoolTimeoutException ignored.");
		} catch (SocketException e){
			Log.d(TAG, "SocketException ignored.");// connection reset by peer / connection timed out
		} catch (SocketTimeoutException e) {
			Log.d(TAG, "SocketException ignored.");
		} catch (Exception e) {//それ以外の例外は報告
			((LogService)context).io.writeLog(e);
		}
		return false;
	}


	//get Activity Types in XML.
	public static String getActivityTypes() {
		if (!checkNetworkAvailable()){
			Log.i(TAG, "Activity types abort. Network unavailable");
			return null;
		}

		Log.i(TAG, "Try to get activity types.");
		String url = prefs.get("login_url");
		HttpGet mGet = new HttpGet(url + "/activity_types.xml");
		try {
			//get method
			HttpResponse response = client.execute(mGet);

			String xml = EntityUtils.toString(response.getEntity(), "UTF-8");
			return xml;

		} catch (ClientProtocolException e) {
			// TODO Auto-generated catch block
			System.out.println("message2:" + e);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			System.out.println("message3:" + e);
		}
		return null;
	}

	//logout
	public static void logout() {
		String url = prefs.get("login_url");
		HttpGet mGet = new HttpGet(url + "/logout");
		try {
			//postの方法で
			HttpResponse responseGet = client.execute(mGet);
			int resGet = responseGet.getStatusLine().getStatusCode();
			loggedIn = false;
			Log.i(TAG, "Logged out with response: " + resGet);
		} catch (ClientProtocolException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}



	public static boolean checkNetworkAvailable() {
		ConnectivityManager connManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);

		boolean result = false;
		try {
			boolean isNetworkAvailable = connManager.getActiveNetworkInfo().isAvailable();
			//Log.v(TAG, "Network available?: " + isNetworkAvailable);

			result = isNetworkAvailable;

		} catch (NullPointerException ex) {
			//Log.v(TAG, "Network available?: No");
		}

		if (!result) loggedIn=false;//networkに繋がってなければloggedinもfalseにしておく

		return result;

	}


}