package com.sozolab.inulog;

import android.app.Application;
import android.content.SharedPreferences;
import android.util.Log;

import com.sozolab.inulog.network.DoPost;
import com.sozolab.inulog.storage.Preference;

/**
 * Created by sozo on 2017/08/08.
 */

public class MyApp extends Application {

    private final String TAG = "MyApp";

    private static SharedPreferences prefs;

    @Override
    public void onCreate() {
        super.onCreate();
        //prefs = new Preference(this);
        prefs = this.getSharedPreferences("prefs", 0);//initialize


        Preference.init(prefs);

        Log.i("MyApp", "login");
        //prefs.edit().putString("login_url", "hoge").commit();

        DoPost.init(this); //initialize static

        Log.i(TAG, prefs.getString("login_url", ""));
    }

}
