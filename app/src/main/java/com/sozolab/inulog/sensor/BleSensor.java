package com.sozolab.inulog.sensor;

import android.annotation.TargetApi;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothAdapter.LeScanCallback;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCallback;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattDescriptor;
import android.bluetooth.BluetoothGattService;
import android.bluetooth.BluetoothManager;
import android.bluetooth.BluetoothProfile;
import android.bluetooth.le.BluetoothLeScanner;
import android.bluetooth.le.ScanCallback;
import android.bluetooth.le.ScanFilter;
import android.bluetooth.le.ScanResult;
import android.bluetooth.le.ScanSettings;
import android.content.Context;
import android.os.Build;
import android.util.Log;

import com.sozolab.inulog.network.UDP;
import com.sozolab.inulog.service.LogService;

import java.net.SocketException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Timer;
import java.util.TimerTask;
import java.util.UUID;

import static java.lang.Math.pow;

/**
 * Created by sozo on 2017/02/27.
 */

public class BleSensor implements LeScanCallback {

    private static final String TAG = "BleSensor";

    //海響館用，場所の表示
    SensorPlaces sensorPlaces = new SensorPlaces();


    private BluetoothLeScanner mBleScanner;
    /**
     * 検索機器の機器名
     */
    private static final String DEVICE_NAME = "CC2650 SensorTag";//sozo
    /**
     * 対象のサービスUUID
     */
    private static final String DEVICE_BUTTON_SENSOR_SERVICE_UUID = "0000ffe0-0000-1000-8000-00805f9b34fb";//F000FFE0-0451-4000-B000-000000000000";//sozo
    /**
     * 対象のキャラクタリスティックUUID
     */
    private static final String DEVICE_BUTTON_SENSOR_CHARACTERISTIC_UUID = "0000ffe1-0000-1000-8000-00805f9b34fb";//"F000FFE1-0451-4000-B000-000000000000";//sozo
    /**
     * キャラクタリスティック設定UUID
     */
    private static final String CLIENT_CHARACTERISTIC_CONFIG = "00002902-0000-1000-8000-00805f9b34fb";
    //private static final String CLIENT_CHARACTERISTIC_CONFIG = "f000aa12-0451-4000-b000-000000000000";//sozo

    //
    //private static final String DEVICE_ACC_SENSOR_SERVICE_UUID = "f000aa82-0451-4000-b000-000000000000";//sozo
    //private static final String DEVICE_ACC_SENSOR_CHARACTERISTIC_UUID = "F000AA81-0451-4000-B000-000000000000";//sozo



    //MOVのUUID(加速度)
    private static final String UUID_MOV_SERV = "f000aa80-0451-4000-b000-000000000000";//sozo
    private static final String UUID_MOV_DATA = "f000aa81-0451-4000-b000-000000000000";//sozo
    private static final String UUID_MOV_CONF = "f000aa82-0451-4000-b000-000000000000"; // 0: disable, bit 0: enable x, bit 1: enable
    private static final String UUID_MOV_PERI = "f000aa83-0451-4000-b000-000000000000";

    //地磁気のUUID
    /*
     private static final String UUID_MAG_SERV = "f000aa30-0451-4000-b000-000000000000";
     private static final String UUID_MAG_DATA = "f000aa31-0451-4000-b000-000000000000";
     private static final String UUID_MAG_CONF = "f000aa32-0451-4000-b000-000000000000"; // 0: disable, 1: enable
     private static final String UUID_MAG_PERI = "f000aa33-0451-4000-b000-000000000000"; // Period in tens of milliseconds
     */

    //赤外線温度のUUID
    private static final String UUID_IRT_SERV = "f000aa00-0451-4000-b000-000000000000";
    private static final String UUID_IRT_DATA = "f000aa01-0451-4000-b000-000000000000";
    private static final String UUID_IRT_CONF = "f000aa02-0451-4000-b000-000000000000"; // 0: disable, 1: enable
    private static final String UUID_IRT_PERI = "f000aa03-0451-4000-b000-000000000000"; // Period in tens of milliseconds

    //角速度のUUID(未入力)
     private static final String UUID_GYR_SERV = "f000aa50-0451-4000-b000-000000000000";
     private static final String UUID_GYR_DATA = "f000aa51-0451-4000-b000-000000000000";
     private static final String UUID_GYR_CONF = "f000aa52-0451-4000-b000-000000000000";

    //照度のUUID
    private static final String UUID_OPT_SERV = "f000aa70-0451-4000-b000-000000000000";
    private static final String UUID_OPT_DATA = "f000aa71-0451-4000-b000-000000000000";
    private static final String UUID_OPT_CONF = "f000aa72-0451-4000-b000-000000000000";
    private static final String UUID_OPT_PERI = "f000aa73-0451-4000-b000-000000000000";

    //湿度のUUID
    private static final String UUID_HUM_SERV = "f000aa20-0451-4000-b000-000000000000";
    private static final String UUID_HUM_DATA = "f000aa21-0451-4000-b000-000000000000";
    private static final String UUID_HUM_CONF = "f000aa22-0451-4000-b000-000000000000";
    private static final String UUID_HUM_PERI = "f000aa23-0451-4000-b000-000000000000";

    //気圧
    private static final String UUID_BAR_SERV = "f000aa40-0451-4000-b000-000000000000";
    private static final String UUID_BAR_DATA = "f000aa41-0451-4000-b000-000000000000";
    private static final String UUID_BAR_CONF = "f000aa42-0451-4000-b000-000000000000";
    private static final String UUID_BAR_PERI = "f000aa44-0451-4000-b000-000000000000";




    //private BleStatus mStatus = BleStatus.DISCONNECTED;
    private BluetoothAdapter mBluetoothAdapter;
    private BluetoothManager mBluetoothManager;
    //    private BluetoothGatt mBluetoothGatt;


    private ArrayList<BluetoothGatt> gatts = new ArrayList<BluetoothGatt>();
    private ArrayList<String> gattAddresses = new ArrayList<String>();

    private static String f0, f1, f2, f3, f4, f5, f6, f7, f8, f9, f10,f11, f12,f13,f14, f15,upload1, upload2, upload3, upload4, upload5, upload6, upload7, upload8, upload9, upload10, upload11, upload12,upload13,upload14,upload15;



    private static Timer restartScanTimer = new Timer();


    LogService context;
    public BleSensor(LogService context){
        this.context = context;
    }

    public void start(){

        mBluetoothManager = (BluetoothManager) context.getSystemService(Context.BLUETOOTH_SERVICE);
        mBluetoothAdapter = mBluetoothManager.getAdapter();

        startScan();//sozo いきなりコネクトするようにした

        startThread();

    }

    //いろんなタイマーやスレッドまとめて起動
    private void startThread(){

        restartScanTimer.schedule(new RestartScanTask(), 0, 10000);//10秒後から10秒おきに閉じる

    }

    protected class RestartScanTask extends TimerTask {
        @TargetApi(Build.VERSION_CODES.LOLLIPOP)
        @Override
        public void run() {
            if (mBleScanner==null)
                mBleScanner = mBluetoothAdapter.getBluetoothLeScanner();
            // デバイスの検出.bluetoothがoffだったらnullになる．

            if (mBleScanner!=null) {
                Log.d(TAG, "StartScan");

                //mBleScanner.stopScan(mScanCallback);
                startScan();
                Log.d(TAG, "StopScan ");

                Log.d(TAG, "RestartScan ");
                mBleScanner.startScan(mScanCallback);

                disconnect();//環境センサのみをセンサデータ接続解除disconnect
            }
        }

    }


    /**
     * BLE 機器との接続を解除する
     */
    private void disconnect(BluetoothGatt gatt) {
        if (gatt != null) {
            Log.d(TAG, "gatt:" + gatt.toString());
            //Log.d(TAG, "gatts:" + gatts);
            gatt.close();
            synchronized(gatts) {
                gatts.remove(gatt);
                gattAddresses.remove(gatt.getDevice().getAddress());
            }
            //Log.d(TAG, "gatts:" + gatts);
            //gatt.getDevice().connectGatt(this, false, mBluetoothGattCallback); //sozo
            //setStatus(BleStatus.CLOSED);
            //scan();

        }
    }


    /**
     * BLE 機器との接続を環境センサの場合のみすべて解除する
     */
    private void disconnect() {
        //Log.d(TAG, "disconnect");
        synchronized(gatts) {//同時アクセスerrorが出ないよう
            for (int i = 0; i < gatts.size(); i++) {
                BluetoothGatt gatt = gatts.get(i);
                String device_id = gatt.getDevice().getAddress();
                Log.d(TAG, "EnvTag?: " +i+" "+  device_id + "　place: " + sensorPlaces.get(device_id) + " EnvSensor?"+ sensorPlaces.isEnv(device_id));
                if (sensorPlaces.isEnv(device_id)) {//環境センサの場合のみ切断 nullの場合は該当しない？
                    Log.d(TAG, "EnvTag disconnect: " + device_id + "　place: " + sensorPlaces.get(device_id));
                    disconnect(gatt);
                }
            }
        }

        //Log.d(TAG, "disconnected");
    }

    ScanCallback mScanCallback;


    private void startScan() {
        if (mBleScanner==null)
            mBleScanner = mBluetoothAdapter.getBluetoothLeScanner();
        // デバイスの検出.bluetoothがoffだったらnullになる．

        // (何もフィルタリングしない) スキャンフィルタの作成
        ArrayList<ScanFilter> mScanFilters = new ArrayList<ScanFilter>();
        // スキャンモードの作成
        ScanSettings.Builder mScanSettingBuiler = new ScanSettings.Builder();
        mScanSettingBuiler.setScanMode(ScanSettings.SCAN_MODE_LOW_POWER);//低電力
        ScanSettings mScanSettings = mScanSettingBuiler.build();

        mScanCallback = new ScanCallback() {
            @Override
            public void onScanResult(int callbackType, ScanResult result) {
                super.onScanResult(callbackType, result);
                // スキャン中に見つかったデバイスに接続を試みる.第三引数には接続後に呼ばれるBluetoothGattCallbackを指定する.
                //                result.getDevice().connectGatt(getApplicationContext(), false, new MyGattCallback());
                //Log.d(TAG, "gattsAddress_ver.5.0:" + result.getScanRecord());

                // advertise ID ----------------------------------------
				/*mBleScanner.stopScan(new ScanCallback() {
					@Override
					public void onScanResult(int callbackType, ScanResult result) {
						super.onScanResult(callbackType, result);
					}
				});*/

                Log.d(TAG, "getdevice: " + result.getDevice().getAddress() + " scanRecord: " +result.getScanRecord());

                if (DEVICE_NAME.equals(result.getScanRecord().getDeviceName())) {

                    //BLEID
                    String message = result.getDevice().getAddress();
                    f14 = writeData("BLEID", f14, message);
                    bleID = message.replaceAll("\n", "") + " " + sensorPlaces.get(result.getDevice().getAddress());//センサタグの場所も

                    BluetoothGatt gatt = result.getDevice().connectGatt(context.getApplicationContext(), false, new MyGattCallback());
                    if (!gattAddresses.contains(gatt.getDevice().getAddress())) {
                        synchronized(gatts) {
                            gatts.add(gatt);
                            gattAddresses.add(gatt.getDevice().getAddress());
                            Log.d(TAG, "gatts added: " + gatt.getDevice().getName() + gatt.getDevice().getAddress() + " # gatts:" + gatts.size());
                        }
                    }

                }
            }

            @Override
            public void onScanFailed(int intErrorCode) {
                super.onScanFailed(intErrorCode);
                Log.d(TAG,"Scan failed");
            }
        };


        if (mBleScanner!=null)
            mBleScanner.startScan(mScanFilters, mScanSettings, mScanCallback);
    }

    public static String deviceName, bleID, bleIrt, bleAcc, bleGyr, bleHum, bleOpt, bleBar, bleMag;

    //見つかったら→ not used?

    @Override
    public void onLeScan(BluetoothDevice device, int rssi, byte[] scanRecord) {
        Log.d(TAG, "device found: " + device.getName());
        if (DEVICE_NAME.equals(device.getName())) {
            deviceName = device.getName();

            //setStatus(BleStatus.DEVICE_FOUND);

            // 省電力のためスキャンを停止する →sozoなし
            //mBluetoothAdapter.stopLeScan(this);
            //mBleScanner.stopScan(mScanCallback);

            //Log.d(TAG, "StartScan");


            //Log.d(TAG, "StartScan2 ");
            //mBleScanner.startScan(mScanCallback);
            //disconnect();


            // GATT接続を試みてgattsに加える
            synchronized(gatts) {
                gatts.add(device.connectGatt(context, false, new MyGattCallback()));//sozo
                Log.d(TAG, "gatts added2: "+ device.getName() + "# gatts:" +gatts.size());
            }

        }
    }


    private class MyGattCallback extends BluetoothGattCallback {
        @Override
        public void onConnectionStateChange(BluetoothGatt gatt, int status, int newState) {
            Log.d(TAG, "onConnectionStateChange: " + status + " -> " + newState);
            if (newState == BluetoothProfile.STATE_CONNECTED && status == BluetoothGatt.GATT_SUCCESS) {
                Log.d(TAG, "onConnectionStateConnected: " + BluetoothProfile.STATE_CONNECTED);
                // GATTへ接続成功
                // サービスを検索する
                gatt.discoverServices();
            } else if (newState == BluetoothProfile.STATE_DISCONNECTED) {
                Log.d(TAG, "onConnectionStateGattFailure: " + BluetoothGatt.GATT_FAILURE);
                // GATT通信から切断された
                //setStatus(BleStatus.DISCONNECTED);
                disconnect(gatt); //sozo　一旦クリアする
                //scan(); //sozo 切断してもscanする

            }
        }



        @Override
        public void onServicesDiscovered(BluetoothGatt gatt, int status) {
            Log.d(TAG, "onServicesDiscovered received: " + status);
            if (status == BluetoothGatt.GATT_SUCCESS) {
                //byte[] value = new byte[] {0x7F,0x00};

                configService(gatt, UUID_IRT_SERV, UUID_IRT_CONF, new byte[]{0x01}, UUID_IRT_PERI ,new byte[]{0x64});
                //configService(gatt, UUID_MOV_SERV, UUID_MOV_CONF, new byte[]{0x7F, 0x00});

            }
        }


        //sozo config書き込み
        private void configService(BluetoothGatt gatt, String UUID_SERV, String UUID_CONF, byte[] value1, String UUID_PERI, byte[] value2) {

            BluetoothGattService service = gatt.getService(UUID.fromString(UUID_SERV));//sozo
            if (service == null) {
                // サービスが見つからなかった
                //setStatus(BleStatus.SERVICE_NOT_FOUND);
            } else {
                // サービスを見つけた
                //setStatus(BleStatus.SERVICE_FOUND);

                BluetoothGattCharacteristic configC =
                        service.getCharacteristic(UUID.fromString(UUID_CONF));//sozo

                if (configC == null) {
                    // キャラクタリスティックが見つからなかった
                    //setStatus(BleStatus.CHARACTERISTIC_NOT_FOUND);
                } else {
                    // キャラクタリスティックを見つけた
                    // config設定する
                    configC.setValue(value1);
                    gatt.writeCharacteristic(configC);


                    if (configC == null){

                    }
                    else{
                        BluetoothGattCharacteristic configD = service.getCharacteristic(UUID.fromString(UUID_PERI));

                        if(configD == null){

                        }
                        else {
                            configD.setValue(value2);
                            gatt.writeCharacteristic(configD);

                            Log.d(TAG,"test ok");
                        }


                    }

                }
            }
            Log.d(TAG, "Service configured:" + UUID_CONF);
        }


        /*@Override
         public void onCharacteristicRead(BluetoothGatt gatt,
         BluetoothGattCharacteristic characteristic,
         int status) {
         Log.d(TAG, "onCharacteristicRead: " + status);
         if (status == BluetoothGatt.GATT_SUCCESS) {
         // READ成功
         }
         }*/

        //sozo configが終わったらdata notificationを設定
        @Override
        public void onCharacteristicWrite(BluetoothGatt gatt,
                                          BluetoothGattCharacteristic characteristic,
                                          int status) {
            Log.d(TAG, "onCharacteristicWrite: " + status);
            if (status == BluetoothGatt.GATT_SUCCESS) {
                // WRITE成功

                if (UUID_MOV_CONF.equals(characteristic.getUuid().toString())) {
                    Log.d(TAG, "MOV_CONF");

                    enableNotification(gatt, UUID_MOV_SERV, UUID_MOV_DATA);
                }

                if (UUID_IRT_CONF.equals(characteristic.getUuid().toString())) {
                    Log.d(TAG, "IRT_CONF");

                    enableNotification(gatt, UUID_IRT_SERV, UUID_IRT_DATA);
                }
                //照度(追加)

                if (UUID_OPT_CONF.equals(characteristic.getUuid().toString())) {
                    Log.d(TAG, "OPT_CONF");

                    enableNotification(gatt, UUID_OPT_SERV, UUID_OPT_DATA);
                }

                //湿度(追加)
                if (UUID_HUM_CONF.equals(characteristic.getUuid().toString())) {
                    Log.d(TAG, "HUM_CONF");

                    enableNotification(gatt, UUID_HUM_SERV, UUID_HUM_DATA);
                }
                if (UUID_BAR_CONF.equals(characteristic.getUuid().toString())) {
                    Log.d(TAG, "BAR_CONF");

                    enableNotification(gatt, UUID_BAR_SERV, UUID_BAR_DATA);
                }

            }

        }


        //sozo
        private void enableNotification(BluetoothGatt gatt, String UUID_SERV, String UUID_DATA) {
            BluetoothGattService serviceC = gatt.getService(UUID.fromString(UUID_SERV));//sozo
            BluetoothGattCharacteristic dataC =
                    serviceC.getCharacteristic(UUID.fromString(UUID_DATA));//sozo


            boolean registered = gatt.setCharacteristicNotification(dataC, true);//sozo

            // Characteristic の Notification 有効化
            BluetoothGattDescriptor descriptor = dataC.getDescriptor(
                    UUID.fromString(CLIENT_CHARACTERISTIC_CONFIG));

            descriptor.setValue(BluetoothGattDescriptor.ENABLE_NOTIFICATION_VALUE);
            gatt.writeDescriptor(descriptor);

            if (registered) {
                // Characteristics通知設定完了
                //setStatus(BleStatus.NOTIFICATION_REGISTERED);
                Log.d(TAG, "Notification registered: Device:" + gatt.getDevice().getAddress());
            } else {
                //setStatus(BleStatus.NOTIFICATION_REGISTER_FAILED);
            }
        }

        //sozo
        @Override
        public void onDescriptorWrite(BluetoothGatt gatt, BluetoothGattDescriptor descriptor, int status) {
            Log.d(TAG, "DescWrite:" + (descriptor.getCharacteristic().getUuid().toString()));
            if (UUID_IRT_DATA.equals(descriptor.getCharacteristic().getUuid().toString())) {
                //IRTのenableNotificationまで終わったらMotionをconfig
                Log.d(TAG, "OPT_CONF");
                configService(gatt, UUID_OPT_SERV, UUID_OPT_CONF, new byte[]{0x01},UUID_OPT_PERI,new byte[]{0x50});


            }
            //湿度(追加)
            if (UUID_OPT_DATA.equals(descriptor.getCharacteristic().getUuid().toString())) {
                //MOVのenableNotificationまで終わったらMotionをconfig
                Log.d(TAG, "HUM_CONF");
                configService(gatt, UUID_HUM_SERV, UUID_HUM_CONF, new byte[]{0x01},UUID_HUM_PERI,new byte[]{0x64});
            }

            if(UUID_HUM_DATA.equals(descriptor.getCharacteristic().getUuid().toString())) {
                //MOVのenableNotificationまで終わったらMotionをconfig
                Log.d(TAG, "BAR_CONF");

                configService(gatt, UUID_BAR_SERV, UUID_BAR_CONF, new byte[]{0x01},UUID_BAR_PERI,new byte[]{0x64});
            }

            //照度(追加)
            if(UUID_BAR_DATA.equals(descriptor.getCharacteristic().getUuid().toString())) {
                //MOVのenableNotificationまで終わったらMotionをconfig
                Log.d(TAG, "MOV_CONF");

                configService(gatt, UUID_MOV_SERV, UUID_MOV_CONF, new byte[]{0x78,0x00},UUID_MOV_PERI,new byte[]{0x64});
            }


            //角速度(追加)  TODO
             /*if(UUID_GYR_DATA.equals(descriptor.getCharacteristic().getUuid().toString())) {
             //MOVのenableNotificationまで終わったらMotionをconfig
             Log.d(TAG, "GYR_CONF");
             configService(gatt, UUID_GYR_SERV, UUID_GYR_CONF, new byte[]{0x7F,0x00});
             }*/
        }


        //データが得られたら
        @Override
        public void onCharacteristicChanged(BluetoothGatt gatt,
                                            BluetoothGattCharacteristic characteristic) {
            Log.d(TAG, "onCharacteristicChanged: " + gatt.getDevice().getAddress()+ " # gatts: "+gatts.size());

            convert(gatt, characteristic);
        }

        /*sozo*/
        private void convert(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic) {
            /*
             if (DEVICE_BUTTON_SENSOR_CHARACTERISTIC_UUID.equals(characteristic.getUuid().toString())) {
             Byte value = characteristic.getValue()[0];
             boolean left = (0 < (value & 0x02));
             boolean right = (0 < (value & 0x01));
             updateButtonState(left, right);
             }*/


            byte[] value = characteristic.getValue();//SensorTagからは2bitの値が渡される
            /*if (UUID_MAG_DATA.equals(characteristic.getUuid().toString())) {
             final float SCALE = (float) (32768 / 4912);
             if (value.length >= 18) {
             int x = (value[13]<<8) + value[12];
             int y = (value[15]<<8) + value[14];
             int z = (value[17]<<8) + value[16];
             Log.d(TAG, "MAG X:"+x+" Y:"+y+" Z:"+z);
             }
             }*/

            String message ="";
            String deviceId = gatt.getDevice().getAddress();

            //加速度データの表示
            if (UUID_MOV_DATA.equals(characteristic.getUuid().toString())) {

                final float SCALE = (float) 4096.0;

                double x = (value[7] << 8) + value[6];
                double y = (value[9] << 8) + value[8];
                double z = (value[11] << 8) + value[10];


                x = (x/SCALE)*-1; y = y/SCALE;  z = (z/SCALE)*-1;
                deviceId = gatt.getDevice().getAddress();

                message = getMessage(deviceId,x,y,z);

                f12 = writeData("BLEACC", f12, message);

                bleAcc = message.replaceAll("\n","")+ sensorPlaces.get(deviceId);

                // 地磁気センサ

                final float MAGSCALE = (float) (32768 / 4912);

                double magx = (value[13] << 8) + value[12];
                double magy = (value[15] << 8) + value[14];
                double magz = (value[17] << 8) + value[16];


                magx = magx/MAGSCALE; magy = magy/MAGSCALE;  magz = magz/MAGSCALE;

                message = getMessage(deviceId, magx, magy, magz);

                f11 = writeData("BLEMAG",f11, message);

                bleMag = message.replaceAll("\n","") + sensorPlaces.get(deviceId);

            }



            //温度データの表示
            if (UUID_IRT_DATA.equals(characteristic.getUuid().toString())) {
                double ambient = extractAmbientTemperature(value);//温度
                double target = extractTargetTemperature(value, ambient);
                double targetNewSensor = extractTargetTemperatureTMP007(value);

                //Log.v(TAG, "IRT ambient:" + ambient + " target:" + target + " target_new:" + targetNewSensor);

                message = getMessage(deviceId, ambient, target, targetNewSensor);
                f8 = writeData("BLEIRT", f8, message);

                //Log.v(TAG, "BLEIRT:" + message);//測定時間の表示
                bleIrt = message.replaceAll("\n","") + " " + sensorPlaces.get(deviceId);
            }
            //照度データの表示	(追加)
            if (UUID_OPT_DATA.equals(characteristic.getUuid().toString())) {
                double optical_v = extractOptical(value);

                //Log.v(TAG, "optical:" + optical_v);

                message = getMessage(deviceId, optical_v);
                f10 = writeData("BLEOPT", f10, message);
                //Log.v(TAG, "BLEOPT:" + message);//測定時間の表示
                bleOpt = message.replaceAll("\n","") + " " + sensorPlaces.get(deviceId);

            }
            //湿度データの表示	(追加)
            if (UUID_HUM_DATA.equals(characteristic.getUuid().toString())) {
                double humidity_v = extractHumidity(value);

                //Log.v(TAG, "humidity:" + humidity_v );

                message = getMessage(deviceId, humidity_v);
                f9 = writeData("BLEHUM", f9, message);
                //Log.v(TAG, "BLEHUM:" + message);//測定時間の表示
                bleHum = message.replaceAll("\n","") + " " + sensorPlaces.get(deviceId);
            }
            //角速度データの表示(追加)
            if (UUID_GYR_DATA.equals(characteristic.getUuid().toString())) {

                message = getMessage(deviceId, extractGyro_x1(value), extractGyro_y1(value), extractGyro_z1(value));
                f15 = writeData("BLEGYR", f15, message);
                bleGyr = message.replaceAll("\n","") + " " + sensorPlaces.get(deviceId);

             }

            //湿度・温度データの表示	(追加)
            if (UUID_BAR_DATA.equals(characteristic.getUuid().toString())) {


                double tem = (value[1] << 8) + value[0];
                //int bar = (value[4] << 8) + value[3];

                double bar1 = extractBar(value);
                message = getMessage(deviceId, tem, bar1);

                //Log.v(TAG, "Temperature:" + tem + "pressure:" + bar1 );

                f13 = writeData("BLEBAR", f13, message);
                //Log.v(TAG, "BLEBAR:" + message);//測定時間の表示
                bleBar = message.replaceAll("\n","") + " " + sensorPlaces.get(deviceId);

            }


        }


    };

    /////sozo 以下は基本的にconvert()の近くに持って行きたいが，staticにして省メモリ化したいのでここで

    ////sozo ここからIRT変換
    private static double extractAmbientTemperature(byte[] v) {
        int offset = 2;
        return shortUnsignedAtOffset(v, offset) / 128.0;
    }

    private static double extractTargetTemperature(byte[] v, double ambient) {
        Integer twoByteValue = shortSignedAtOffset(v, 0);

        double Vobj2 = twoByteValue.doubleValue();
        Vobj2 *= 0.00000015625;

        double Tdie = ambient + 273.15;

        double S0 = 5.593E-14; // Calibration factor
        double a1 = 1.75E-3;
        double a2 = -1.678E-5;
        double b0 = -2.94E-5;
        double b1 = -5.7E-7;
        double b2 = 4.63E-9;
        double c2 = 13.4;
        double Tref = 298.15;
        double S = S0 * (1 + a1 * (Tdie - Tref) + a2 * pow((Tdie - Tref), 2));
        double Vos = b0 + b1 * (Tdie - Tref) + b2 * pow((Tdie - Tref), 2);
        double fObj = (Vobj2 - Vos) + c2 * pow((Vobj2 - Vos), 2);
        double tObj = pow(pow(Tdie, 4) + (fObj / S), .25);

        return tObj - 273.15;
    }

    private static double extractTargetTemperatureTMP007(byte[] v) {
        int offset = 0;
        return shortUnsignedAtOffset(v, offset) / 128.0;
    }

    private static Integer shortSignedAtOffset(byte[] c, int offset) {
        Integer lowerByte = (int) c[offset] & 0xFF;
        Integer upperByte = (int) c[offset + 1]; // // Interpret MSB as signed
        return (upperByte << 8) + lowerByte;
    }

    private static Integer shortUnsignedAtOffset(byte[] c, int offset) {
        Integer lowerByte = (int) c[offset] & 0xFF;
        Integer upperByte = (int) c[offset + 1] & 0xFF;
        return (upperByte << 8) + lowerByte;
    }

    ///// ここまでIRT変換

    //HUM変換(追加)

    public static double extractHumidity(byte[] v) {
        int a = shortUnsignedAtOffset(v, 2);

        // bits [1..0] are status bits and need to be cleared according
        // to the userguide, but the iOS code doesn't bother. It should
        // have minimal impact.
        a = a - (a % 4);

        return (-6f) + 125f * (a / 65535f);
    }


    //OPT変換(追加)

    public static double extractOptical(byte [] value) {
        int mantissa;
        int exponent;
        Integer sfloat= shortUnsignedAtOffset(value, 0);
        mantissa = sfloat & 0x0FFF;
        exponent = (sfloat >> 12) & 0xFF;
        double output;

        double magnitude = pow(2.0f, exponent);

        output = (mantissa * magnitude);
        return (output / 100.0f);

    }

    public static double extractBar(byte[] value) {

        // Sensor
        double sensor = 0.0d;
        if (value.length > 4) {
            Integer val = twentyFourBitUnsignedAtOffset(value, 2);
            sensor = (double) val / 100.0;
        } else {
            int mantissa;
            int exponent;
            Integer sfloat = shortUnsignedAtOffset(value, 2);

            mantissa = sfloat & 0x0FFF;
            exponent = (sfloat >> 12) & 0xFF;

            double output;
            double magnitude = pow(2.0f, exponent);
            output = (mantissa * magnitude);
            sensor = output / 100.0f;
        }
        return sensor;
    }

    private static Integer twentyFourBitUnsignedAtOffset(byte[] c, int offset) {
        Integer lowerByte = (int) c[offset] & 0xFF;
        Integer mediumByte = (int) c[offset + 1] & 0xFF;
        Integer upperByte = (int) c[offset + 2] & 0xFF;
        return (upperByte << 16) + (mediumByte << 8) + lowerByte;
    }


    //GYR変換(追加)

     public static float extractGyro_y1(byte[] v) {

     float y1 = shortSignedAtOffset(v, 0) * (500f / 65536f) * -1;
     return y1;
     }

     public static float extractGyro_x1(byte[] v) {
     float x1 = shortSignedAtOffset(v, 2) * (500f / 65536f);
     return x1;
     }


     public static float extractGyro_z1(byte[] v) {
     float z1 = shortSignedAtOffset(v, 4) * (500f / 65536f);
     return z1;
     }




    /**
     * Save the file and send via UDP
     * @param filePostfix latter part of the filename
     * @param type  sensor type
     * @return filePostfix
     **/
    private String writeData(String type, String filePostfix, String message) {
        return(context.io.writeData(type, filePostfix, message));
    }

    private String getMessage(String v1, double v2, double v3, double v4){
        return v1 + "," + v2 + "," + v3 + "," + v4;
    }

    private String getMessage(String v1, double v2, double v3){
        return v1 + "," + v2 + ","+ v3;
    }

    private String getMessage(String v1, double v2){
        return v1 + "," + v2;
    }



}
