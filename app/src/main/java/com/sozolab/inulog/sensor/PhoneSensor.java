package com.sozolab.inulog.sensor;

import android.content.Context;
import android.content.SharedPreferences;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.util.Log;
import com.sozolab.inulog.activity.MainActivity;
import com.sozolab.inulog.network.UDP;
import com.sozolab.inulog.service.LogService;
import com.sozolab.inulog.storage.Preference;

import java.io.File;
import java.io.FileOutputStream;
import java.net.SocketException;
import java.text.DecimalFormat;
import java.util.Calendar;
import java.util.prefs.Preferences;


/**
 * Created by sozo on 2017/02/27.
 */

public class PhoneSensor implements SensorEventListener {

    private static String TAG = "PhoneSensor";

    private static Preference prefs;

    private static LogService context;

    public PhoneSensor(LogService context){
        this.context = context;
        //prefs = context.getSharedPreferences("syllabus", 0);//こちらでも初期化

        prefs.setIfEmpty("Acc_on", true); //default on

    }

    private static SensorManager sm1, sm2, sm3, sm4, sm5, sm6, sm7, sm8;


    public void start() {

        Log.d(TAG, "start");


        /* まずは全てonにする */
        //Acc
        sm1 = (SensorManager) context.getSystemService(Context.SENSOR_SERVICE);

        if (sm1 != null) {
            boolean result = sm1.registerListener(this,
                    sm1.getDefaultSensor(Sensor.TYPE_ACCELEROMETER),
                    SensorManager.SENSOR_DELAY_NORMAL);
            Log.i(TAG, "Sensor Acc registered "+result);
        }

        //Mag
        sm2 = (SensorManager) context.getSystemService(Context.SENSOR_SERVICE);
        if (sm2!=null) {
            boolean result = sm2.registerListener(this,
                    sm2.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD),
                    SensorManager.SENSOR_DELAY_NORMAL);

            Log.i(TAG, "Sensor MAG registered "+result);
        }

        //Ori
        sm3 = (SensorManager) context.getSystemService(Context.SENSOR_SERVICE);
        if (sm3 != null) {
            boolean result = sm3.registerListener(this,
                    sm3.getDefaultSensor(Sensor.TYPE_ORIENTATION),
                    SensorManager.SENSOR_DELAY_NORMAL);
            Log.i(TAG, "Sensor Ori registered "+result);
        }

        //Gyro
        sm4 = (SensorManager) context.getSystemService(Context.SENSOR_SERVICE);
        if (sm4 != null) {

            boolean result = sm4.registerListener(this,
                    sm4.getDefaultSensor(Sensor.TYPE_GYROSCOPE),
                    SensorManager.SENSOR_DELAY_NORMAL);
            Log.i(TAG, "Sensor Gyro registered "+result);
        }

        //Light
        sm5 = (SensorManager) context.getSystemService(Context.SENSOR_SERVICE);
        if (sm5!=null) {

            boolean result =sm5.registerListener(this,
                    sm5.getDefaultSensor(Sensor.TYPE_LIGHT),
                    SensorManager.SENSOR_DELAY_NORMAL);
            Log.i(TAG, "Sensor Light registered "+ result);
        }


    }

    public static boolean doWrite = true;
    private static String f1, f2, f3, f4, f5, f6, f7;

    public static String Acc, Mag, Ori, Gyro, Light, Hum, Tem;


    boolean acc_on, mag_on, ori_on, gyro_on, light_on, hum_on, tem_on;


    @Override
    // -------------------------------smartphone sensor------------------------------------------------------------------
    public void onSensorChanged(SensorEvent event) {

        acc_on = prefs.getBoolean("Acc_on");
        mag_on = prefs.getBoolean("Mag_on");
        ori_on = prefs.getBoolean("Ori_on");
        gyro_on = prefs.getBoolean("Gyro_on");
        light_on = prefs.getBoolean("Light_on");
        hum_on = prefs.getBoolean("Hum_on");
        tem_on = prefs.getBoolean("Tem_on");



        String message = "";//new String();
        //String values ="";

        //String timeStr =  formatCurrentMillis();
        DecimalFormat df = new DecimalFormat("#,##0.000");// format for decimals
        //Log.v(TAG, ""+event);

        if (doWrite) {
            switch (event.sensor.getType()) {
                case Sensor.TYPE_ACCELEROMETER:
                    //Log.v(TAG, ""+event.values.length);
                    if (!acc_on) break; //acc_onでなければ終了
                    message = getMessage(event, 3);
                    Acc = message;
                    String udpMessage = getMessage(event, 3, 1.0/9.8);
                    //Log.v(TAG, ""+message);

                    f1 = writeData("acc", f1, message, udpMessage);//change to gravity for UDP
                    break;

                case Sensor.TYPE_MAGNETIC_FIELD:
                    if (!mag_on) break;
                    message = getMessage(event, 3);
                    Mag = message;
                    f2 = writeData("mag", f2, message);
                    break;

                case Sensor.TYPE_ORIENTATION:
                    if(!ori_on) break;
                    message = getMessage(event, 3);
                    Ori = message;
                    f3 = writeData("ori", f3, message);
                    break;

                case Sensor.TYPE_GYROSCOPE:
                    if(!gyro_on) break;
                    message = getMessage(event, 3);
                    Gyro = message;
                    f4 = writeData("gyro", f4, message);
                    break;

                case Sensor.TYPE_LIGHT:
                    if(!light_on) break;
                    message = getMessage(event, 1);
                    Light = message;
                    f5 = writeData("light", f5, message);
                    break;

                case Sensor.TYPE_AMBIENT_TEMPERATURE:
                    if(!tem_on) break;
                    message = getMessage(event, 3);
                    Tem = message;
                    f6 = writeData("temp", f6, message);
                    break;

                case Sensor.TYPE_RELATIVE_HUMIDITY:
                    if(!hum_on) break;
                    message = getMessage(event, 3);
                    Hum = message;
                    f7 = writeData("hum", f7, message);
                    break;

            }


        }
    }
    //-------------------------------------------------------------------------------------------------------------------

    public String writeData(String type, String filePostfix, String values, String udpValues) {
        return context.io.writeData(type, filePostfix, values, udpValues);
    }

    public String writeData(String type, String filePostfix, String values) {
        return context.io.writeData(type, filePostfix, values, values);
    }


    private String getMessage(SensorEvent event, int numValues){
        return getMessage(event, numValues, 1.0);
    }


    private String getMessage(SensorEvent event, int numValues, double udpRatio) {
        //Log.v(TAG, ""+event.values.length);
        DecimalFormat df = new DecimalFormat("#,##0.000");// format for decimals
        String values = df.format(event.values[0]*udpRatio);
        for (int i=1;i<numValues;i++)
            values += "," + df.format(event.values[i]*udpRatio);

        return values;
   }



    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {
    }


}
