package com.sozolab.inulog.sensor;

import java.util.HashMap;

/**
 * Created by sozo on 2017/02/27.
 */

public class SensorPlaces {

        private HashMap<String, String> places;
        private HashMap<String, Boolean> envs;

        SensorPlaces() {
            places = new HashMap<String, String>();
            envs = new HashMap<String, Boolean>();
            init();
        }


    private void init(){
        this.put("24:71:89:C0:C5:03","1", false);
        this.put("24:71:89:C0:99:07","2", false);
        this.put("24:71:89:C0:A7:02","3", false);
        this.put("24:71:89:C0:B5:00","5", false);
        this.put("24:71:89:1A:4D:80","6", false);
        this.put("24:71:89:19:64:05","7", false);
        this.put("24:71:89:C0:BC:83","8", false);
        this.put("24:71:89:C0:8F:01","9", false);
        this.put("24:71:89:C0:75:03","10", false);
        this.put("24:71:89:C0:C6:80","11", false);
        this.put("24:71:89:C0:D4:04","12", false);
        this.put("24:71:89:C0:59:85","13", false);
        this.put("24:71:89:C0:88:80","14", false);
        this.put("24:71:89:19:63:81","15", false);
        this.put("24:71:89:1A:08:83","16", false);
        this.put("24:71:89:C0:BE:04","17", false);
        this.put("24:71:89:C1:4A:82","18", false);
        this.put("24:71:89:1A:09:03","19", false);
        this.put("24:71:89:C0:C2:82","20", false);
        this.put("24:71:89:C0:42:81","21", false);
        this.put("24:71:89:C0:D5:00","22", false);

        this.put("24:71:89:1A:55:06","23", false);
        this.put("24:71:89:C1:53:83","24", false);
        this.put("24:71:89:C0:57:86","25", false);
        this.put("24:71:89:19:65:82","26", false);
        this.put("24:71:89:C0:7F:86","27", false);
        this.put("24:71:89:C0:C5:81","4", false);
        this.put("24:71:89:C0:B9:86","エントランスホール");
        this.put("24:71:89:C0:21:87","居室201");
        this.put("24:71:89:C0:61:05","居室202");
        this.put("24:71:89:C0:59:81","居室203");
        this.put("B0:B4:48:D2:EE:85","居室205");
        this.put("24:71:89:C0:BA:84","居室206");
        this.put("24:71:89:C0:31:05","居室210");
        this.put("24:71:89:C0:D0:04","居室212");
        this.put("24:71:89:19:66:85","居室213");
        this.put("24:71:89:C0:C4:85","2F管理事務室");
        this.put("24:71:89:C0:C2:87","2Fステーション");
        this.put("B0:B4:48:D6:BA:87","2F食堂");
        this.put("24:71:89:C0:5C:06","2F特殊浴室");
        this.put("24:71:89:C0:57:80","2F汚物処理室");
        this.put("24:71:89:C0:20:83","2F汚物洗濯室");
        this.put("24:71:89:C0:9A:81","2F健康管理室");
        this.put("B0:B4:48:D3:09:85 ","居室301");
        this.put("24:71:89:C0:8B:86","居室302");
        this.put("24:71:89:C0:A2:80","居室303");
        this.put("24:71:89:C0:1C:83","居室305");
        this.put("24:71:89:1A:4D:00","居室307");
        this.put("24:71:89:C0:94:06","居室310");
        this.put("24:71:89:C0:B4:80","居室311");
        this.put("24:71:89:C1:50:86","居室313");
        this.put("24:71:89:C0:C4:84","居室315");
        this.put("24:71:89:C0:B6:04","居室316");
        this.put("24:71:89:C0:B5:84","居室317");
        this.put("B0:B4:48:D7:24:07","居室318");
        this.put("24:71:89:C0:CF:80","居室321");
        this.put("24:71:89:C0:4F:86","3Fステーション");
        this.put("24:71:89:C0:61:83","3F食堂");
        this.put("24:71:89:1A:11:06","3F食堂流し場");
        this.put("24:71:89:C0:F4:02","3F浴室");
        this.put("24:71:89:C0:B4:82","3F汚物処理室");
        this.put("24:71:89:C0:C1:03","3F汚物洗濯室");
        this.put("24:71:89:C0:88:83","居室401");
        this.put("B0:B4:48:D2:FF:06","居室402");
        this.put("24:71:89:1A:14:04","居室405");
        this.put("24:71:89:C1:50:80","居室407");
        this.put("24:71:89:C0:9B:85","居室408");
        this.put("24:71:89:C0:57:00","居室410");
        this.put("24:71:89:19:51:82","居室412");
        this.put("24:71:89:C0:7C:02","居室415");
        this.put("24:71:89:C0:98:85","居室417");
        this.put("24:71:89:C0:56:81","居室420");
        this.put("24:71:89:C0:A0:06","居室421");
        this.put("24:71:89:C0:74:07","居室422");
        this.put("24:71:89:C0:72:07","4Fステーション");
        this.put("24:71:89:C0:C7:06","4F食堂");
        this.put("B0:B4:48:D7:5F:04","4F食堂流し場");
        this.put("24:71:89:1A:19:03","4F汚物処理室");
        this.put("24:71:89:C0:44:81","4F汚物洗濯室");
        this.put("24:71:89:C1:53:02","居室501");
        this.put("B0:B4:48:D6:C9:02","居室502");
        this.put("B0:B4:48:D2:EA:84","居室503");
        this.put("B0:B4:48:D6:C0:81","居室505");
        this.put("B0:B4:48:C9:A5:01","居室506");
        this.put("B0:B4:48:C9:B4:02","居室507");
        this.put("B0:B4:48:D2:E7:81","居室508");
        this.put("B0:B4:48:D3:04:87","居室511");
        this.put("B0:B4:48:D6:BB:01","居室512");
        this.put("B0:B4:48:D2:E1:86","居室515");
        this.put("B0:B4:48:D6:C7:86","居室522");
        this.put("B0:B4:48:D6:C4:81","居室523");
        this.put("B0:B4:48:C9:D7:02","5Fステーション");
        this.put("B0:B4:48:D6:C7:04","5F食堂");
        this.put("B0:B4:48:C9:C9:04","5F食堂流し場");
        this.put("B0:B4:48:C9:A4:03","5F汚物処理室");
        this.put("B0:B4:48:D2:F9:81","5F汚物洗濯室");
        this.put("B0:B4:48:C9:E1:83","居室416");
        this.put("B0:B4:48:D6:B1:84","居室306");
        this.put("B0:B4:48:D7:2A:81","居室418");
        this.put("B0:B4:48:D7:2E:07","居室411");
        this.put("B0:B4:48:D7:5D:00","居室308");
        this.put("B0:B4:48:C9:D1:04","居室207");
        this.put("B0:B4:48:D2:F0:07","居室208");
        this.put("24:71:89:C0:C0:87","白石タグ",false);
        this.put("24:71:89:C0:C2:83","創造タグ", false);

    }


    public void put(String device_id, String place, boolean isEnv){
            places.put(device_id, place);
            envs.put(device_id, isEnv);
        }

        public void put(String device_id, String place){
            put(device_id, place, true);
        }


        public String get(String device_id){
            return places.get(device_id);
        }

        public boolean isEnv(String device_id) {
            Boolean res = envs.get(device_id);
            if (res == null) return false;
            return (boolean) res;
        }


}
