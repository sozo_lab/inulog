package com.sozolab.inulog.activity;


import com.google.android.gms.appindexing.Action;
import com.google.android.gms.appindexing.AppIndex;
import com.google.android.gms.common.api.GoogleApiClient;
import com.sozolab.domain.ActivityType;
import com.sozolab.inulog.MyApp;
import com.sozolab.inulog.R;
import com.sozolab.inulog.appControl.BootBroadcastReceiver;
import com.sozolab.inulog.network.DoPost;
import com.sozolab.inulog.sensor.BleSensor;
import com.sozolab.inulog.sensor.PhoneSensor;
import com.sozolab.inulog.service.LogService;
import com.sozolab.inulog.storage.FileIO;
import com.sozolab.inulog.storage.Preference;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.PowerManager;
import android.os.RemoteException;
import android.os.StrictMode;
import android.text.Html;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.method.LinkMovementMethod;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.util.Xml;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.view.WindowManager.LayoutParams;

import java.io.StringReader;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Timer;
import java.util.TimerTask;

import android.content.IntentFilter;
import android.widget.Toast;
import android.widget.ToggleButton;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import static android.support.v4.content.WakefulBroadcastReceiver.startWakefulService;


public class MainActivity extends Activity implements ServiceConnection, CompoundButton.OnCheckedChangeListener {


	//private final static int SDKVER_LOLLIPOP = 21;
	//private TextView mStatusText;


	private Button toLogin;


	public FileIO io;
	private static Preference prefs;



	private static final String TAG="MainActivity";

	//staticが重要
	//public static String def_login_url = "https://test.eneact.sozolab.jp";
	//public static String def_upload_url = "https://test.eneact.sozolab.jp";

	/**
	 * ATTENTION: This was auto-generated to implement the App Indexing API.
	 * See https://g.co/AppIndexing/AndroidStudio for more information.
	 */
	private GoogleApiClient client;

	public static DoPost post;

	private static Timer closeWindowTimer=new Timer(), setTextTimer=new Timer(), clearTextTimer = new Timer();

	private SensingActivity sensingActivity = new SensingActivity();


	BootBroadcastReceiver broadcastReceiver;

	PowerManager.WakeLock wl; //class memberにしておかないとA/PowerManager: WakeLock finalized while still heldとなる

	public void onCreate(Bundle savedInstanceState) {
		Log.i("Status", "onCreate");
		StrictMode.setThreadPolicy(new StrictMode.ThreadPolicy.Builder()
				//.detectDiskReads().detectDiskWrites()
				.detectNetwork().penaltyLog().build());

		/*StrictMode.setVmPolicy(new StrictMode.VmPolicy.Builder()
				.detectLeakedSqlLiteObjects().penaltyLog().penaltyDeath()
				.build());*/

		super.onCreate(savedInstanceState);

		io = new FileIO(this);


		//指定していない例外をcatchして，再起動!
		if(true) {
		//if(false) {
			Thread.setDefaultUncaughtExceptionHandler(new Thread.UncaughtExceptionHandler() {
				@Override
				public void uncaughtException(Thread thread, Throwable t) {
					Log.i("UncaughtException", t.toString());//測定時間の表示

					//落ちたらexceptionをサーバーに送ってみる
					io.writeLog(t);

					try {//5秒待って
						Thread.sleep(5000);
					} catch (InterruptedException e) {
					}
					// activity再起動を先にする
					restart();

					// Activity終了
					finish();
					android.os.Process.killProcess(android.os.Process.myPid());
				}
			});
		}


		//prefs = getSharedPreferences("syllabus", 0);//こちらでも初期化
		//sp = getSharedPreferences("userInfo", Context.MODE_PRIVATE);

		//post = new DoPost(this);


		//Wifiに再接続するため，ディープスリープしない．
		PowerManager pm = (PowerManager) getSystemService(Context.POWER_SERVICE);
		wl = pm.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, "InuLog");
		wl.acquire();




		// ブロードキャストリスナー//画面on off
		broadcastReceiver = new BootBroadcastReceiver();
		broadcastReceiver.init(this);//効率化したい
		// リスナーの登録
		registerReceiver(broadcastReceiver, new IntentFilter(Intent.ACTION_SCREEN_ON));
		registerReceiver(broadcastReceiver, new IntentFilter(Intent.ACTION_SCREEN_OFF));

		findViews();
		sensingActivity.findViews();

		createView();





		setEventListener();


		/*
		int page_count = sp.getInt("PAGE_COUNT", 1);
		if (page_count == 2) {

			//この後画面を閉じるメッセージ
			// 	dialog_return();
			closeWindow();


		}
		*/


		// ATTENTION: This was auto-generated to implement the App Indexing API.
		// See https://g.co/AppIndexing/AndroidStudio for more information.
		client = new GoogleApiClient.Builder(this).addApi(AppIndex.API).build();



		//最後にタイマータスク
		startThread();
		sensingActivity.startThread();

		startWakefulService(this, new Intent(this, LogService.class));


		// サービスにバインド //これだけで終了時に再起動？
		bindService(new Intent(this, LogService.class), this, Context.BIND_AUTO_CREATE);

		alarm(this);

	}

	private TextView title, url_link2, ActivityTypes;

	int[] labelButtonIds = {R.id.labelButton1, R.id.labelButton2, R.id.labelButton3,
		R.id.labelButton4,R.id.labelButton5,R.id.labelButton6,R.id.labelButton7,
		R.id.labelButton8,R.id.labelButton9,R.id.labelButton10,R.id.labelButton11,
		R.id.labelButton12,R.id.labelButton13,R.id.labelButton14,R.id.labelButton15,
		R.id.labelButton16,R.id.labelButton17,R.id.labelButton18,R.id.labelButton19,
		R.id.labelButton20,R.id.labelButton21,R.id.labelButton22};

	private static int numButton = 22;

	ToggleButton[] labels = new ToggleButton[numButton];


	ArrayList<ActivityType> actTypes;

	private void findViews(){
        setContentView(R.layout.main_activity);

        title = (TextView) findViewById(R.id.title);
        toLogin = (Button) findViewById(R.id.toLogin);

        //	scan = (Button) findViewById(R.id.scan);

        url_link2 = (TextView) findViewById(R.id.url_link2);

		toLogin = (Button) findViewById(R.id.toLogin);


		for (int i=0;i<numButton;i++) {
			labels[i] = (ToggleButton) this.findViewById(labelButtonIds[i]);
		}


		//ActivityTypes = (TextView) this.findViewById(R.id.ActivityTypes);

	}


    //viewを作る
    private void createView(){
        //Layout
        this.getWindow().setSoftInputMode(LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        //title->INULog
        String tl = "InuLog";
        SpannableStringBuilder style = new SpannableStringBuilder(tl);
        //SpannableStringBuilder Achieve CharSequence Interface
        style.setSpan(new ForegroundColorSpan(Color.rgb(0, 153, 51)), 0, 1, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        style.setSpan(new ForegroundColorSpan(Color.BLACK), 1, 3, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        style.setSpan(new ForegroundColorSpan(Color.rgb(255, 0, 51)), 3, 4, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        style.setSpan(new ForegroundColorSpan(Color.BLACK), 4, 5, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        title.setText(style);
        url_link2.setMovementMethod(LinkMovementMethod.getInstance());
        Spanned text = Html.fromHtml("<a href=\""+DoPost.def_upload_url+"\">"+DoPost.def_login_url+"</a>");
//		Spanned text = Html.fromHtml("<a href=\" http://test.eneact.sozolab.jp/\">test.eneact.sozolab.jp</a>");
//		Spanned text = Html.fromHtml("<a href=\" http://210.152.12.159/\">210.152.12.159</a>");

        //tmp url_link2.setText(text);
        url_link2.setText("https://sozolab.jp/");


		//ActivityTypes.setText(prefs.get("ActivityTypes"));

		actTypes = ActivityType.getAll();

		int n = actTypes.size() < numButton ? actTypes.size() : numButton; //minimum

		for(int i=0;i<n;i++){
			String buttonText = actTypes.get(i).name;
			labels[i].setText(buttonText);
			labels[i].setTextOn(buttonText);
			labels[i].setTextOff(buttonText);
		}

	}


	private void setEventListener(){
		/*toLogin.setOnLongClickListener(new View.OnLongClickListener() {
			@Override
			public boolean onLongClick(View v) {

				//throw new IllegalStateException("Manual Exception for debug."); //わざとexception発生
				moveToSensingActivity();

				return true;
				//return false;
			}
		});*/

		toLogin.setOnClickListener(new View.OnClickListener(){
			@Override
			public void onClick(View v) {

				if (v.getId() == R.id.toLogin) {
					//toLogin.setEnabled(false);

					//loginに進む
					moveToLoginActivity();
					//dialog();
					//toLogin.setEnabled(true);
				}


				/*File[] files = getDataFiles();
				if (files.length==0) return;
				int s = new Random().nextInt(files.length);//乱数
				String delete_filename = files[s].getName();
				delete(delete_filename);

				Log.d("Inulog", "delete filename: " + delete_filename+"remained: "+files.length);*/

			}
		});


		for(int i=0;i<numButton;i++){
			labels[i].setOnCheckedChangeListener(this);
		}

	}




	/* サービスに接続されたとき	 */

	@Override
	public void onServiceConnected(ComponentName className, IBinder service) {
		Log.d(TAG, "LogService connected.");
		messenger = new Messenger(service);
		//replyMessenger = new Messenger(new ReplyHandler(getApplicationContext()));
	}

	@Override
	public void onServiceDisconnected(ComponentName className) {

		}


		String labelfile;

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        // ToggleButton1が押された場合

        if (Arrays.asList(labelButtonIds).contains(buttonView.getId())){

			int ix = Arrays.asList(labelButtonIds).indexOf(buttonView.getId());

			ActivityType actType = actTypes.get(ix);

			Toast.makeText(this, "Clicked " + actType.name + " " + isChecked,
					Toast.LENGTH_LONG).show();

			labels[ix].setText(actType.name);

			String message = actType.id+","+actType.name+"," + isChecked;
			Log.v(TAG, message);

			labelfile = io.writeData(labelfile, "label", message);

		}
    }





	static class ReplyHandler extends Handler {

		private Context _cont;

		public ReplyHandler(Context cont) {
			_cont = cont;
		}

		@Override
		public void handleMessage(Message msg) {
			switch(msg.what) {
				case 2:
					Toast.makeText(_cont, (String)msg.obj, Toast.LENGTH_SHORT).show();
					break;
			}
		}
	}

	private Messenger messenger, replyMessenger;


	public void sendMessage() {
		try {
			// メッセージにBundleを付与して送信
			//Bundle arg = new Bundle();
			Message msg = Message.obtain(null, 3);
			//msg.setData(arg);

			// コールバックを設定
			msg.replyTo = replyMessenger;

			if(messenger!= null)
				messenger.send(msg);

		} catch (RemoteException e) {
			e.printStackTrace();
			Toast.makeText(getApplicationContext(), "メッセージの送信に失敗しました。", Toast.LENGTH_SHORT).show();
		}
	}


	public void restartService() {
		Log.d(TAG, "sendMessage");
		try {
			// メッセージにBundleを付与して送信
			//Bundle arg = new Bundle();
			Message msg = Message.obtain(null, 1);
			//msg.setData(arg);

			if(messenger!=null) {//messenger is init.
				messenger.send(msg);
				Log.d(TAG, "message sent");
			}

		} catch (RemoteException e) {
			e.printStackTrace();
			Log.d(TAG, "restartService failed.");

		}
	}





	//いろんなタイマーやスレッドまとめて起動
	private void startThread(){

		//closeWindowTimer.schedule(new CloseWindowTask(), 120000, 120000);//120秒後から120秒おきに閉じる


	}


	//画面を閉じる
	private void closeWindow(){
		Intent intent = new Intent();
		intent.setAction(Intent.ACTION_MAIN);
		intent.addCategory(Intent.CATEGORY_HOME);
		startActivity(intent);
	}

	//(再)起動
	public void restart(){
		Intent startIntent = new Intent(this, MainActivity.class);
		startActivity(startIntent);

	}


	private void moveToLoginActivity(){
		Intent startIntent = new Intent(this, LoginActivity.class);
		startIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

		startActivity(startIntent);
	}




	void alarm(Context cnt){
		// intent 設定で自分自身のクラスを設定
		Intent mainActivity = new Intent(cnt, MainActivity.class);
		// PendingIntent , ID=0
		PendingIntent pendingIntent = PendingIntent.getActivity(cnt, 0, mainActivity, PendingIntent.FLAG_CANCEL_CURRENT);
		// AlarmManager のインスタンス生成

		AlarmManager alarmManager = (AlarmManager)cnt.getSystemService(Context.ALARM_SERVICE);
		alarmManager.setRepeating(AlarmManager.ELAPSED_REALTIME_WAKEUP, System.currentTimeMillis(), 60000, pendingIntent);
		//60秒おきにとりあえず起こす．これでアプリが死なない？
	}

	public void onDestroy(){//強制終了したら再度スタート??効かない？
		Log.i("Status", "Destroyed");

		unregisterReceiver(broadcastReceiver);

		restart();//再度スタート

		super.onDestroy();


	}



	@Override
	public void onStart() {
		Log.i("Status", "onStart");
		super.onStart();

		client.connect();
		// ATTENTION: This was auto-generated to implement the App Indexing API.
		// See https://g.co/AppIndexing/AndroidStudio for more information.
		Action viewAction = Action.newAction(
				Action.TYPE_VIEW, // TODO: choose an action type.
				"Main Page", // TODO: Define a title for the content shown.
				// TODO: If you have web page content that matches this app activity's content,
				// make sure this auto-generated web page URL is correct.
				// Otherwise, set the URL to null.
				Uri.parse("http://host/path"),
				// TODO: Make sure this aucto-generated app deep link URI is correct.
				Uri.parse("android-app://com.sozolab.inulog/http/host/path")
		);
		AppIndex.AppIndexApi.start(client, viewAction);


		/*try {
			udpSocket = new DatagramSocket(5555);
		} catch (Exception e) {e.printStackTrace();}
		*/

	}


	@Override
	public void onResume(){
		Log.i("Status", "onResume");
		super.onResume();
	}

	@Override
	public void onStop() {
		Log.i("Status", "Stop....zzz.");
		super.onStop();

		// ATTENTION: This was auto-generated to implement the App Indexing API.
		// See https://g.co/AppIndexing/AndroidStudio for more information.
		Action viewAction = Action.newAction(
				Action.TYPE_VIEW, // TODO: choose an action type.
				"Main Page", // TODO: Define a title for the content shown.
				// TODO: If you have web page content that matches this app activity's content,
				// make sure this auto-generated web page URL is correct.
				// Otherwise, set the URL to null.
				Uri.parse("http://host/path"),
				// TODO: Make sure this auto-generated app deep link URI is correct.
				Uri.parse("android-app://com.sozolab.inulog/http/host/path")
		);
		AppIndex.AppIndexApi.end(client, viewAction);
		client.disconnect();
	}



	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if (keyCode == KeyEvent.KEYCODE_BACK && event.getRepeatCount() == 0) {
			closeWindow();
			//dialog();
			return false;
		}
		return false;
	}

	public void onPause() {
		Log.i("Status", "Pause....zzz.");
		super.onPause();

		//finish(); debug

	}






	//一定時間後に閉じるタスクの内部クラス
	protected class CloseWindowTask extends TimerTask{
		public void run(){
			closeWindow();
		}
	}




	private class SensingActivity {

		private TextView DeviceAddress, BleOpt, BleIrt, BleHum, BleGyr, BleAcc, BleId, BleBar, BleMag;


		private TextView Acc, Mag, Ori, Gyro, Light, Pressure, Tem, Hum, Proximity, WRSta, title, url_link2, mStatusText, FileNumber;
		private Button toLogin;
		private Handler mHandler = new Handler();




		private void findViews(){
			//setContentView(R.layout.sensing_activity);
			title = (TextView) findViewById(R.id.title);
			WRSta = (TextView) findViewById(R.id.Station);
			toLogin = (Button) findViewById(R.id.toLogin);
			url_link2 = (TextView) findViewById(R.id.url_link2);

			Acc = (TextView) findViewById(R.id.Acc);
			Mag = (TextView) findViewById(R.id.Mag);
			Ori = (TextView) findViewById(R.id.Ori);
			Gyro = (TextView) findViewById(R.id.Gyro);
			Light = (TextView) findViewById(R.id.Light);
			Tem = (TextView) findViewById(R.id.Tem);
			Hum = (TextView) findViewById(R.id.Hum);

			DeviceAddress = (TextView) findViewById(R.id.deviceadd);

			BleIrt = (TextView) findViewById(R.id.bleirt);
			BleAcc = (TextView) findViewById(R.id.bleacc);
			BleGyr = (TextView) findViewById(R.id.blegyr);
			BleHum = (TextView) findViewById(R.id.blehum);
			BleOpt = (TextView) findViewById(R.id.bleopt);
			BleId =  (TextView) findViewById(R.id.bleid);
			BleBar =  (TextView) findViewById(R.id.blebar);
			BleMag =  (TextView) findViewById(R.id.blemag);


			FileNumber = (TextView) findViewById(R.id.fileNumber);
			//messageView = (TextView) this.findViewById(R.id.message);
		}

		private Timer setTextTimer=new Timer(), clearTextTimer = new Timer();

		//いろんなタイマーやスレッドまとめて起動
		private void startThread(){
			setTextTimer.schedule(new SetTextTask(), 0, 100); 	 //0.1秒ごと
			//clearTextTimer.schedule(new ClearTextTask(), 0, 1000); 	 //1秒ごと

		}



		String deviceName="";

		protected class SetTextTask extends TimerTask{

			public void run(){
				mHandler.post(new Runnable() {
					public void run() {
						DeviceAddress.setText("DeviceName : " + deviceName);
						FileNumber.setText("Files: " + LogService.numfiles + " Size: "+ LogService.filesize + " Count: " + LogService.filecount + " Buffer: "+LogService.lines);
						String loggedInMessage = DoPost.loggedIn ? "ログインしています" : "センシング中";//tmp "ログインしていません！"
						toLogin.setText(loggedInMessage);
						Acc.setText("Acc: " + PhoneSensor.Acc);
						Mag.setText("Mag: " + PhoneSensor.Mag);
						Ori.setText("Ori: " + PhoneSensor.Ori);
						Gyro.setText("Gyro: " + PhoneSensor.Gyro);
						Light.setText("Light: " + PhoneSensor.Light);
						Tem.setText("Tem: " + PhoneSensor.Tem);
						Hum.setText("Hum: " + PhoneSensor.Hum);

						BleId.setText("BleID: " + BleSensor.bleID);
						BleIrt.setText("BleIrt: " + BleSensor.bleIrt);
						BleAcc.setText("BleAcc: " + BleSensor.bleAcc);
						BleGyr.setText("BleGyr: " + BleSensor.bleGyr);
						BleHum.setText("BleHum: " + BleSensor.bleHum);
						BleOpt.setText("BleOpt: " + BleSensor.bleOpt);
						BleBar.setText("BleBar: " + BleSensor.bleBar);
						BleMag.setText("BleMag: " + BleSensor.bleMag);

					}
				});

			}
		}


		protected class ClearTextTask extends TimerTask{

			public void run(){
				mHandler.post(new Runnable() {
					public void run() {
						Acc.setText("Acc: ");
						Mag.setText("Mag: ");
						Ori.setText("Ori: ");
						Gyro.setText("Gyro: ");
						Light.setText("Light: ");
						Tem.setText("Tem: ");
						Hum.setText("Hum: ");
						BleId.setText("BleID: " );
						BleIrt.setText("BleIrt: ");
						BleAcc.setText("BleAcc: ");
						BleGyr.setText("BleGyr: ");
						BleHum.setText("BleHum: ");
						BleOpt.setText("BleOpt: ");
						BleBar.setText("BleBar: ");
						BleMag.setText("BleMag: ");
					}
				});

			}
		}

	}




}


