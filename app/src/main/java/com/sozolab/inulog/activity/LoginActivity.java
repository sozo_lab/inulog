package com.sozolab.inulog.activity;


import java.security.MessageDigest;
//import java.net.CookieStore;
import java.util.HashMap;

/*
import org.apache.http.client.CookieStore;
import org.apache.http.cookie.Cookie;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;
*/

import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.text.Html;
import android.text.InputType;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.method.LinkMovementMethod;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnLongClickListener;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.sozolab.inulog.MyApp;
import com.sozolab.inulog.R;
import com.sozolab.inulog.storage.Preference;


public class LoginActivity extends Activity {
	private TextView net,title,url_link1;
    EditText username;
	EditText password;
	ImageButton login;
	//Button register;
	//private CheckBox rem_pw, auto_login;
	LinearLayout l;
	public static HashMap<String, Object> map; 
	private HashMap<String, String> session = new HashMap<String,String>(); 
	private boolean  flag3; 
	public MessageDigest md = null;
	public static String user;
	public static String pass;

    private static Preference prefs;


	@Override
	protected void onCreate(Bundle savedInstanceState) {
		/*StrictMode.setThreadPolicy(new StrictMode.ThreadPolicy.Builder()
		.detectDiskReads().detectDiskWrites().detectNetwork()
		.penaltyLog().build());*/

		/*StrictMode.setVmPolicy(new StrictMode.VmPolicy.Builder()
		.detectLeakedSqlLiteObjects().penaltyLog().penaltyDeath()
		.build());*/

		super.onCreate(savedInstanceState);
		setContentView(R.layout.login_activity);

		findViews();


        username.setText(prefs.get("USER_NAME"));
        password.setText(prefs.get("PASSWORD"));


        //Log.d("Inulog",MainActivity.sp.getString("page", "not_auto"));
		/*if(MainActivity.sp.getString("page", "not_auto")!=null){
			//自動ログイン
		          //rem_pw.setChecked(true);  
		          username.setText(MainActivity.sp.getString("USER_NAME", ""));
		          password.setText(MainActivity.sp.getString("PASSWORD", ""));
		          //自動的にログインする状態をチェックする 
		          Log.d("Inulog", "AUTO_ISCHECK:"+ MainActivity.sp.getBoolean("AUTO_ISCHECK", true));

                if(first) //アプリ起動直後
                {
                      login();
                      first = false;
                }

		}*/
	}
	@Override 
    public boolean onKeyDown(int keyCode, KeyEvent event) { 
        //if ((keyCode == KeyEvent.KEYCODE_BACK || keyCode == KeyEvent.KEYCODE_HOME) && event.getRepeatCount() == 0) {
        if (event.getRepeatCount() == 0) {
            //dialog();
            login.setEnabled(false);
            Toast.makeText(this, "ログイン試行中", Toast.LENGTH_SHORT).show();
            closeWindow();

            login();//それでもログイントライ!

            return false; 
        }
        return false;
    }



    //画面を閉じる
    private void closeWindow(){
        Intent intent = new Intent();
        intent.setAction(Intent.ACTION_MAIN);
        intent.addCategory(Intent.CATEGORY_HOME);
        startActivity(intent);
    }

	protected void dialog() { 
        AlertDialog.Builder builder = new Builder(LoginActivity.this); 
        builder.setMessage("アプリをシャットダウンしますか？"); 
        builder.setTitle("メッセージ"); 
        builder.setPositiveButton("Yes", 
                new android.content.DialogInterface.OnClickListener() { 
                    @Override 
                    public void onClick(DialogInterface dialog, int which) { 
                        dialog.dismiss(); 
                        LoginActivity.this.finish();
                    } 
                }); 
        builder.setNegativeButton("No", 
                new android.content.DialogInterface.OnClickListener() { 
                    @Override 
                    public void onClick(DialogInterface dialog, int which) { 
                        dialog.dismiss(); 
                    } 
                }); 
        builder.create().show(); 
    } 
	
	private void findViews() {

		String tl = "INULog";
        SpannableStringBuilder style=new SpannableStringBuilder(tl); 
        //SpannableStringBuilder Achieve CharSequence Interface  
        style.setSpan(new ForegroundColorSpan(Color.rgb(0, 153, 51)), 0, 1,Spannable.SPAN_EXCLUSIVE_EXCLUSIVE ); 
        style.setSpan(new ForegroundColorSpan(Color.BLACK), 1, 3,Spannable.SPAN_EXCLUSIVE_EXCLUSIVE ); 
        style.setSpan(new ForegroundColorSpan(Color.rgb(255,0,51)), 3, 4,Spannable.SPAN_EXCLUSIVE_EXCLUSIVE ); 
        style.setSpan(new ForegroundColorSpan(Color.BLACK), 4, 5,Spannable.SPAN_EXCLUSIVE_EXCLUSIVE );
        title = (TextView)findViewById(R.id.title); 
        title.setText(style);
		
        //url_link
        url_link1 = (TextView)findViewById(R.id.url_link1); 
        url_link1.setMovementMethod(LinkMovementMethod.getInstance());
        Spanned text = Html.fromHtml("<a href=\"http://eneact-test.apps.salt-dev.space/\">server_url</a>");
        url_link1.setText(text);
        
		username=(EditText) findViewById(R.id.username);
		password=(EditText) findViewById(R.id.password);
//		login=(Button) findViewById(R.id.login);
		login= (ImageButton)findViewById(R.id.login);
//		register=(Button) findViewById(R.id.register);
		password.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
	    //backgroundLayoutをタッチ
		l = (LinearLayout) findViewById(R.id.backgroundLayout); 
//	    rem_pw = (CheckBox) findViewById(R.id.rem);  
//        auto_login = (CheckBox) findViewById(R.id.auto);
	    //長押す
	    login.setOnLongClickListener(new OnLongClickListener(){
	    	public boolean onLongClick(View v) {

	    		Intent intent = new Intent(LoginActivity.this,
	                    SettingActivity.class);
	            startActivity(intent);	
	            LoginActivity.this.finish();
	            return false;
	            
	    	}
	    });
	    //押す
		login.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
                login();
			}
		});
	
		l.setOnClickListener(new OnClickListener() {   
	        @Override   
	        public void onClick(View v) {   
	        ((InputMethodManager) getSystemService(INPUT_METHOD_SERVICE))   
	        .hideSoftInputFromWindow(getCurrentFocus().getWindowToken(),                                
	        		InputMethodManager.HIDE_NOT_ALWAYS);   
	                        }   
	                }); 
		
      //自動的にログイン  
        /*auto_login.setOnCheckedChangeListener(new OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView,boolean isChecked) {  
                if (auto_login.isChecked()) {  
                    Log.d("Inulog", "AUTO_ISCHECK選択");
                    MainActivity.sp.edit().putBoolean("AUTO_ISCHECK", true).commit();

                } else {  
                    Log.d("Inulog", "AUTO_ISCHECK解除");
                    MainActivity.sp.edit().putBoolean("AUTO_ISCHECK", false).commit();

                }

            }  
        });*/
	}

    private void login(){

        login.setEnabled(false);
        Toast.makeText(this, "ログイン試行中", Toast.LENGTH_SHORT).show();
        //自動的にログインを設置する
        //auto_login.setChecked(true);
        user = username.getText().toString();
        pass = password.getText().toString();


        //login
        try{

            /*if(a.login(user,pass)){
                Log.d("Inulog", "Logged in.");
                Toast.makeText(LoginActivity.this, "ログイン成功", Toast.LENGTH_SHORT).show();
                if (MainActivity.sp!=null) {//Mainactivityがcreateされていないことがあるので．
                    Editor editor = MainActivity.sp.edit();
                    editor.putString("USER_NAME", user);
                    editor.putString("PASSWORD", pass);
                    editor.putString("page", "auto");
                    editor.putInt("PAGE_COUNT", 2);//遷移後にdialogを出すため
//                    editor.putString("COOKIE",cookie.getValue());
                    editor.commit();
                }


            }else{
                Log.i("TAG","error");
                Toast.makeText(LoginActivity.this, "ログイン失敗", Toast.LENGTH_LONG).show();
                login.setEnabled(true);

            }*/
            //どちらにしてもMainActivityに移動してすぐLogin

            moveToMainActivity();//ログインできなくても計測開始する
        }catch(Exception e){
            Toast.makeText(this, "ログイン時例外", Toast.LENGTH_LONG).show();
        }
    }

    private void moveToMainActivity(){

        prefs.set("USER_NAME", user);
        prefs.set("PASSWORD", pass);
        //editor.putString("page", "auto");

        //画面遷移
        Intent intent = new Intent(LoginActivity.this, MainActivity.class);
        startActivity(intent);
        //if (MainActivity.sp!=null)//MainActivityがcreateされていないことがあるので．
            prefs.set("PAGE_COUNT", 2);//遷移後にdialogを出すため
        finish();
    }

}

