package com.sozolab.inulog.activity;


import com.sozolab.inulog.MyApp;
import com.sozolab.inulog.R;
import com.sozolab.inulog.network.DoPost;
import com.sozolab.inulog.network.UDP;
import com.sozolab.inulog.sensor.PhoneSensor;
import com.sozolab.inulog.storage.Preference;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.os.StrictMode;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.TextView;
import android.widget.Toast;

public class SettingActivity extends Activity  {
	private TextView note,text_login_url,text_upload_url ;
	private CheckBox Acc,Mag, Ori, Gyro,Light,Pressure,Tem,Proximity,Hum,udp_on;
	private Button OK, activityTypes;
	EditText login_url,upload_url, udp_address, udp_port;


	private static Preference prefs;

	
	public void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);


		setContentView(R.layout.setting_activity);
		
		note = (TextView)this.findViewById(R.id.note);
		text_login_url = (TextView)this.findViewById(R.id.text_login_url );
		text_upload_url = (TextView)this.findViewById(R.id.text_upload_url);
		Acc = (CheckBox)this.findViewById(R.id.Acc);
		Mag = (CheckBox)this.findViewById(R.id.Mag);
		Ori = (CheckBox)this.findViewById(R.id.Ori);
		Gyro = (CheckBox)this.findViewById(R.id.Gyro);
		Light = (CheckBox)this.findViewById(R.id.Light);
		Tem = (CheckBox)this.findViewById(R.id.Tem);
		Hum = (CheckBox)this.findViewById(R.id.Hum);
		/*Pressure = (CheckBox)this.findViewById(R.id.Pressure);
		Tem = (CheckBox)this.findViewById(R.id.Tem);
		Proximity = (CheckBox)this.findViewById(R.id.Proximity);*/
		login_url = (EditText)this.findViewById(R.id.login_url);
		upload_url = (EditText)this.findViewById(R.id.upload_url);


        udp_on = (CheckBox)this.findViewById(R.id.udp_check);
        udp_address = (EditText)this.findViewById(R.id.udp_address);
        udp_port = (EditText)this.findViewById(R.id.udp_port);

		OK = (Button)this.findViewById(R.id.OK);

		activityTypes = (Button)this.findViewById(R.id.updateActivityTypesButton);


		//prefs = getSharedPreferences("syllabus", 0);//こちらでも初期化



		Acc.setChecked(prefs.getBoolean("Acc_on"));
		Mag.setChecked(prefs.getBoolean("Mag_on"));
		Ori.setChecked(prefs.getBoolean("Ori_on"));
		Gyro.setChecked(prefs.getBoolean("Gyro_on"));
		Light.setChecked(prefs.getBoolean("Light_on"));
		Tem.setChecked(prefs.getBoolean("Tem_on"));
		Hum.setChecked(prefs.getBoolean("Hum_on"));

		/*
		Pressure.setChecked(prefs.getBoolean("Pressure_on" ,false));
		Proximity.setChecked(prefs.getBoolean("Proximity_on" ,false));
		*/

		prefs.setIfEmpty("udp_on", UDP.doSend);
		udp_on.setChecked(prefs.getBoolean("udp_on"));

		Acc.setOnCheckedChangeListener(new OnCheckedChangeListener(){
			@Override
			public void onCheckedChanged(CompoundButton buttonView,
					boolean check) {
				prefs.set("Acc_on", check);
			}
		});
		
		Mag.setOnCheckedChangeListener(new OnCheckedChangeListener(){
			@Override
			public void onCheckedChanged(CompoundButton buttonView,
					boolean check) {
				prefs.set("Mag_on", check);
  		}
		});
		
		Ori.setOnCheckedChangeListener(new OnCheckedChangeListener(){
			@Override
			public void onCheckedChanged(CompoundButton buttonView,
					boolean check) {
				prefs.set("Ori_on", check);
			}
		});
		
		Gyro.setOnCheckedChangeListener(new OnCheckedChangeListener(){
			@Override
			public void onCheckedChanged(CompoundButton buttonView,
					boolean check) {
				prefs.set("Gyro_on", check);
			}
		});
		
		Light.setOnCheckedChangeListener(new OnCheckedChangeListener(){
			@Override
			public void onCheckedChanged(CompoundButton buttonView,
					boolean check) {
				prefs.set("Light_on", check);
			}
		});
		
		Tem.setOnCheckedChangeListener(new OnCheckedChangeListener(){
			@Override
			public void onCheckedChanged(CompoundButton buttonView,
					boolean check) {
				prefs.set("Tem_on", check);
			}
		});
		Hum.setOnCheckedChangeListener(new OnCheckedChangeListener(){
			@Override
			public void onCheckedChanged(CompoundButton buttonView,
					boolean check) {
				prefs.set("Hum_on", check);
			}
		});
		udp_on.setOnCheckedChangeListener(new OnCheckedChangeListener(){
			@Override
			public void onCheckedChanged(CompoundButton buttonView,
										 boolean check) {
				//UDP.doSend=check;

				prefs.set("udp_on", check);

				Log.d("UDP", "udp_on: " + check);
			}
		});


		login_url.setText(prefs.get("login_url"));
		upload_url.setText(prefs.get("upload_url"));




		//UDPのデフォルトを現在のIPから取得．
		String default_address = "";
		//try {
			WifiManager wifiManager = (WifiManager) getApplicationContext().getSystemService(WIFI_SERVICE);
			WifiInfo wifiInfo = wifiManager.getConnectionInfo();
			int ip = wifiInfo.getIpAddress();
			default_address =
					((ip >> 0) & 0xFF) + "." + ((ip >> 8) & 0xFF) + "." + ((ip >> 16) & 0xFF) + "." + "255";
			//((ip >> 24) & 0xFF);

		//} catch (Exception e) {e.printStackTrace();}


		//UDP
		prefs.setIfEmpty("udp_address", default_address);
		String udp_address_text = prefs.get("udp_address");
		Log.i("UDP", "address:"+udp_address_text);
        udp_address.setText(udp_address_text);

		prefs.setIfEmpty("udp_port", ""+UDP.port);
        udp_port.setText(""+ prefs.get("udp_port"));

		/*Pressure.setOnCheckedChangeListener(new OnCheckedChangeListener(){
			@Override
			public void onCheckedChanged(CompoundButton buttonView,
					boolean isChecked) {
				// TODO Auto-generated method stub
				if(Pressure.isChecked()){
					checked6 = true;

				}else{
					checked6 = false;
				}
				Editor editor = getSharedPreferences("syllabus", 0).edit();
			 	editor.putBoolean("Pressure_on", isChecked);
			 	editor.commit();
			}
		});
		
		Tem.setOnCheckedChangeListener(new OnCheckedChangeListener(){
			@Override
			public void onCheckedChanged(CompoundButton buttonView,
					boolean isChecked) {
				// TODO Auto-generated method stub
				if(Tem.isChecked()){
					checked7 = true;

				}else{
					checked7 = false;
				}
				Editor editor = getSharedPreferences("syllabus", 0).edit();
			 	editor.putBoolean("Tem_on", isChecked);
			 	editor.commit();
			}
		});
		
		
		
		Proximity.setOnCheckedChangeListener(new OnCheckedChangeListener(){
			@Override
			public void onCheckedChanged(CompoundButton buttonView,
					boolean isChecked) {
				// TODO Auto-generated method stub
				if(Proximity.isChecked()){
					checked8 = true;

				}else{
					checked8 = false;
				}
				Editor editor = getSharedPreferences("syllabus", 0).edit();
			 	editor.putBoolean("Proximity_on", isChecked);
			 	editor.commit();
			}
		});
		*/
		
		OK.setOnClickListener(new OnClickListener(){
			public void onClick (View v){
				//login_url 
				try{
				    if(("").equals(login_url.getText().toString())){
				        prefs.set("login_url", DoPost.def_login_url);
				    }else {
                        prefs.set("login_url", login_url.getText().toString());
                    }
				}catch(Exception e){
				}
				//upload_url
				try{
				    if(("").equals(upload_url.getText().toString())){
				        prefs.set("upload_url", DoPost.def_upload_url);
				    }else {
						prefs.set("upload_url", upload_url.getText().toString());
                    }
				}catch(Exception e){
				}

				//UDP
				if(!("").equals(udp_address.getText().toString())){
                    UDP.address = udp_address.getText().toString();
					prefs.set("udp_address", udp_address.getText().toString());
					Log.i("UDP", UDP.address);
                }
                if(!("").equals(udp_port.getText().toString())){
                    UDP.port = Integer.valueOf(udp_port.getText().toString());
					prefs.set("udp_port", udp_port.getText().toString());
					Log.i("UDP", ""+UDP.port);
                }



                //System.out.println("login_url:"+login_url.getText().toString());
				//System.out.println("upload_url:"+upload_url.getText().toString());
				/*new AlertDialog.Builder(SettingActivity.this)
				.setTitle("設定")
				.setMessage("設定完了しました！")
				.setPositiveButton("OK",new android.content.DialogInterface.OnClickListener(){
					public void onClick(DialogInterface arg0, int arg1){
						arg0.dismiss();
						Intent startIntent = new Intent(SettingActivity.this,
			                    LoginActivity.class);
						 startActivity(startIntent);
						 SettingActivity.this.finish();
					}
				})
				.show();*/

				Toast.makeText(SettingActivity.this, "設定完了しました！", Toast.LENGTH_SHORT).show();
				Intent startIntent = new Intent(SettingActivity.this,
						LoginActivity.class);
				startActivity(startIntent);

			}
		});

		activityTypes.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				new AlertDialog.Builder(SettingActivity.this)
					.setTitle("Update Activity types?")
					.setMessage("This may renew the names of the previous activitie logs.")
					.setPositiveButton("OK", new DialogInterface.OnClickListener() {
						@Override
						public void onClick(DialogInterface dialog, int which) {
							//if (MainActivity.post == null) return;
							String xml = DoPost.getActivityTypes();
							//Log.d("ActivityTypes", ""+xml);

							if (xml == null){
								Toast.makeText(SettingActivity.this, "Failed to update activity types.", Toast.LENGTH_LONG);
							} else {
								prefs.set("ActivityTypes", xml);


								Toast.makeText(SettingActivity.this, "Success to update activity types!", Toast.LENGTH_LONG);
							}
						}
					})
					.setNegativeButton("Cancel", null)
					.show();
				}
		});

	}
}
