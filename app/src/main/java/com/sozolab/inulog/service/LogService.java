package com.sozolab.inulog.service;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.wifi.WifiManager;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.os.Vibrator;
import android.util.Log;

import com.sozolab.inulog.activity.MainActivity;
import com.sozolab.inulog.network.DoPost;
import com.sozolab.inulog.sensor.BleSensor;
import com.sozolab.inulog.sensor.PhoneSensor;
import com.sozolab.inulog.storage.FileIO;
import com.sozolab.inulog.storage.Preference;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FilenameFilter;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.text.DecimalFormat;
import java.util.Calendar;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;



/**
 * Created by sozo on 2017/02/27.
 */

public class LogService extends Service {
    final static String TAG = "LogService";


    DoPost post;

    PhoneSensor phoneSensor;
    BleSensor bleSensor;

    public FileIO io;
    private Preference prefs;

    @Override
    public void onCreate() {
        Log.d(TAG, "onCreate");
        super.onCreate();

        io = new FileIO(this);

        deleteLockFiles();

        messenger = new Messenger(new LogServiceHandler(getApplicationContext()));

        //post = new DoPost(this);

        phoneSensor = new PhoneSensor(this);
        phoneSensor.start();

        bleSensor = new BleSensor(this);
        bleSensor.start();


        startThread();

        alarm(this);

    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.d(TAG, "onStartCommand");
        return START_REDELIVER_INTENT;//強制終了した場合再起動
    }

    @Override
    public IBinder onBind(Intent intent) {
        Log.d(TAG, "onBind");
        return messenger.getBinder();
    }

    @Override
    public void onRebind(Intent intent) {
        Log.d(TAG, "onRebind");
    }


    @Override
    public boolean onUnbind(Intent intent) {
        Log.d(TAG, "onUnbind");


        return true;//次回bind時にonRebindが呼ばれる．
    }

    @Override public void onDestroy() {
        super.onDestroy();
        Log.d(TAG, "onDestroy");
    }


    private static Timer vibrationTimer = new Timer(), wifiTimer= new Timer(), loginTimer=new Timer(),
            restartScanTimer = new Timer();
    private static int numThreads = 5;
    private static Thread[] uploadThreads = new Thread[numThreads];//1 threads

    //いろんなタイマーやスレッドまとめて起動
    private void startThread(){
        //vibratorを30分おきに  nullかどうか調べないとoncreateのたびに呼ばれてしまう．
        //vibrationTimer.schedule(new VibrationTask(), 1000, 1800000);//30秒後から30分おき．

        //Wifiを5分おきに再起動
        //wifiTimer.schedule(new WifiTask(), 30000, 300000);//30秒後から5分おき．

        //ログインしていなければ時々ログインを試みる．
        loginTimer.schedule(new LoginTask(), 1000, 60000);//1秒後から2分おき．


        for (int i=0;i<numThreads;i++) {
            uploadThreads[i] = new Thread(new UploadTask());
            uploadThreads[i].start();//アップロードスレッド10こ
        }

        /*
        restartScanTimer.schedule(new MainActivity.restartScanTask(), 0, 10000);//10秒後から10秒おきに閉じる
        */


    }


    Vibrator vibrator;
    //バイブレーター
    protected class VibrationTask extends TimerTask {
        @Override
        public void run(){
            vibrator = (Vibrator)getSystemService(VIBRATOR_SERVICE);
            vibrator.vibrate(4000);//4秒
            //long pattern[] = {1000, 200, 700, 200, 400, 200 };
            //vibrator.vibrate(pattern, 0);
        }
    }


    //wifiをoff/on
    protected class WifiTask extends TimerTask{

        @Override
        public void run(){
            WifiManager wifi = (WifiManager)getSystemService(WIFI_SERVICE);
            try {
                if (!post.checkNetworkAvailable()) {
                    wifi.setWifiEnabled(false);
                    Log.d("Network", "Wifi disabled.");
                    wifi.setWifiEnabled(true);
                    Log.d("Network", "Wifi enabled.");
                }
            }catch (Exception e){Log.d(TAG,e.getMessage());}

        }
    }


    protected class LoginTask extends TimerTask{
        @Override
        public void run() {
            if (!post.loggedIn) {//ログインしてなければ
                Log.d(TAG, "Timer login");
                post.login(prefs.get("USER_NAME"), prefs.get("PASSWORD"));
            }
        }
    }

    /* inner class */
    /*public class LogServiceBinder extends Binder {
        public LogService getService() {
            return LogService.this;
        }
    }*/

    //uploadのためのクラス
    protected class UploadTask extends TimerTask {
        //private String content;


        public void run() {
            File[] files;

            while (true) {
                files = getUnlockedDataFiles();//netにつながっていてもnumfilesを更新したいので，これだけは実行する．．
                Log.v(TAG, "Remained files to upload: " + files.length);

                if (post.checkNetworkAvailable() && post.loggedIn) {


                    if (files.length > 0) {
                        int i = new Random().nextInt(files.length);//乱数
                        String upload_filename = files[i].getName();
                        //Log.d(TAG, "upload filename: " + upload_filename);

                        try {
                            //読んだ分ずつdoPost
                            if (readAndPost(upload_filename, 0)) {

                                Log.v(TAG, "Success to upload and delete: " + upload_filename);
                                //content = null;
                                delete(upload_filename);


                            } else {
                                //Log.v(TAG, "Failed to upload:" + upload_filename);
                            }

                        } catch (Exception e) {
                            System.out.println("error");
                        }

                        //System.out.println(sp.getString("upload_url", def_upload_url) + "/sensors/0/upload");
                    }

                }

            }

        }




        protected boolean readAndPost(String filename, int typepos){
            int max_size = 100100;

            File lockfile = new File(getFilesDir(), filename + ".lock");

            try {
                Log.d(TAG, "Try readAndPost "+filename);
                String type = filename.split("_")[typepos];
                //Log.d(TAG, "type:"+type);


                Log.v(TAG, "create "+lockfile.getName());
                lockfile.createNewFile();// make lock file

                FileInputStream fin = openFileInput(filename); //
                filesize = new File(getFilesDir(), filename).length();

                BufferedReader reader = new BufferedReader(new InputStreamReader(fin, "UTF-8"));
                StringBuilder sb1 = new StringBuilder();
                String s;

                filecount = 1;

                while ((s = reader.readLine()) != null) {
                    synchronized (sb1) {
                        sb1.append(s + "\n");
                    }

                    lines = sb1.length();

                    if (lines > max_size) { //オーバーしたら分割

                        Log.d(TAG, "readAndPost filecount with split : " + filecount + " buffer: " + lines + " type: " + type);

                        post.doPost(
                                prefs.get("upload_url") + "/sensors/0/upload",
                                "filename", filename,
                                "type", type,
                                "version", getVersionName(),
                                "data", new String(sb1)
                        );

                        filecount++;
                        sb1 = new StringBuilder();

                    }

                }

                Log.d(TAG, "readAndPost filecount : " + filecount + " buffer: " + lines + " type: " + type);
                post.doPost(
                        prefs.get("upload_url") + "/sensors/0/upload",
                        "filename", filename,
                        "type", type,
                        "version", getVersionName(),
                        "data", new String(sb1)
                );


                reader.close();
                fin.close();

                Log.v(TAG, "delete "+lockfile.getName());

                lockfile.delete(); //delete lock file
                return true;
            } catch (FileNotFoundException e) {
                Log.d(TAG, "FileNotFoundException but ignored "+filename);//ignore because other threads will have read.

            } catch (Exception e) {
                lockfile.delete(); //delete lock file
                Log.d(TAG, "Exception in uploading "+ filename);
                e.printStackTrace();
            }


            return false;

        }

    }
    public static int numfiles=0, filecount=0, lines = 0;
    public static long filesize = 0;

    protected boolean delete(String filename){
        return(new File(getFilesDir(),filename).delete());
    }


    public File[] getDataFiles() {


        File[] files = new File(getFilesDir(), "/").listFiles(new FilenameFilter() {
            @Override
            public boolean accept(File dir, String filename) {
                return filename.endsWith(".csv");// && !filename.startsWith("upload");//uploadではじまってない
            }
        });


        numfiles = files.length;
        return (files);
    }

    public File[] getUnlockedDataFiles() {

        File[] files = new File(getFilesDir(), "/").listFiles(new FilenameFilter() {
            @Override
            public boolean accept(File dir, String filename) {
                File lockfile = new File(dir, filename + ".lock");
                return filename.endsWith(".csv") && !lockfile.exists(); // no lockfile.
            }
        });


        numfiles = files.length;
        return (files);
    }



    private void deleteLockFiles(){
        File[] files = new File(getFilesDir(), "/").listFiles(new FilenameFilter() {
            @Override
            public boolean accept(File dir, String filename) {
                return filename.endsWith(".lock");//ends with .lock
            }
        });

        for(int i=0;i<files.length;i++){
            Log.d(TAG, "delete: "+files[i].getName());
            files[i].delete();
        }

    }


    private Messenger messenger;
    /* メッセージ受け取り */
    static class LogServiceHandler extends Handler {

        private Context _cont;

        public LogServiceHandler(Context cont) {
            _cont = cont;
        }

        @Override
        public void handleMessage(Message msg) {
            Log.d(TAG, "receiveMessage");
            Messenger reply = msg.replyTo;
            if(reply!=null) {
                try {
                    switch (msg.what) {
                        case ACC:
                            //Toast.makeText(_cont, (String) msg.obj, Toast.LENGTH_SHORT).show();
                            reply.send(Message.obtain(null, msg.what, "受信が終わりましたyo"));
                            break;
                        case MAG:
                            reply.send(Message.obtain(null, msg.what, "受信が終わりましたyo"));
                            break;
                        case ORI:
                            reply.send(Message.obtain(null, msg.what, "受信が終わりましたyo"));
                            Log.d(TAG, "3");
                            break;
                        case GYRO:
                            break;
                        case LIGHT:
                            break;
                        case PRESSURE:
                            break;
                        case TEM:
                            break;
                        case HUM:
                            break;
                        case Proximity:
                            break;
                        case WRSta:
                            break;
                        case FileNumber:
                            break;
                        case DeviceAddress:
                            break;
                        case BleOpt:
                            break;
                        case BleIrt:
                            break;
                        case BleHum:
                            break;
                        case BleGyr:
                            break;
                        case BleAcc:
                            break;
                        case BleId:
                            break;
                        case BleBar:
                            break;
                        case BleMag:
                            break;

                        default:
                            Log.d(TAG, "Received unknown what " + msg.what);
                            super.handleMessage(msg);
                    }
                } catch (RemoteException e) {
                    e.printStackTrace();
                }
            }
        }

    }


    void alarm(Context cnt){
        // intent 設定で自分自身のクラスを設定
        Intent mainActivity = new Intent(cnt, LogService.class);
        // PendingIntent , ID=0
        PendingIntent pendingIntent = PendingIntent.getActivity(cnt, 0, mainActivity, PendingIntent.FLAG_CANCEL_CURRENT);
        // AlarmManager のインスタンス生成

        AlarmManager alarmManager = (AlarmManager)cnt.getSystemService(Context.ALARM_SERVICE);
        alarmManager.setRepeating(AlarmManager.ELAPSED_REALTIME_WAKEUP, System.currentTimeMillis(), 60000, pendingIntent);
        //60秒おきにとりあえず起こす．これでアプリが死なない？
    }

    public static final int ACC=0, MAG=1, ORI=2, GYRO=3, LIGHT=4, PRESSURE=5,  TEM=6, HUM=7, Proximity=8, WRSta=9, FileNumber=10;
    public static final int DeviceAddress=11, BleOpt=12, BleIrt=13, BleHum=14, BleGyr=15, BleAcc=16, BleId=17, BleBar=18, BleMag=19;


    private static String f0, f1, f2, f3, f4, f5, f6, f7, f8, f9, f10,f11, f12,f13,f14, f15,upload1, upload2, upload3, upload4, upload5, upload6, upload7, upload8, upload9, upload10, upload11, upload12,upload13,upload14,upload15;


    public String getVersionName() throws Exception {
// packagemanager
        PackageManager packageManager = getPackageManager();
// getPackageName()，
        PackageInfo packInfo = packageManager.getPackageInfo(getPackageName(), 0);
        String version = packInfo.versionName;
        return version;
    }


}
