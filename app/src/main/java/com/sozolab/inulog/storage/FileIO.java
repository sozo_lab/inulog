package com.sozolab.inulog.storage;

import android.content.Context;
import android.util.Log;
import com.sozolab.inulog.network.UDP;

import java.io.File;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.net.SocketException;
import java.text.DecimalFormat;
import java.util.Calendar;

/**
 * Created by sozo on 2017/02/27.
 */

public class FileIO {

    private static final String TAG = "FileIO";

    Context context;
    public FileIO(Context context){
        this.context = context;
    }

    public String formatCurrentTime() {
        return formatCurrentTime(false);
    }

    public String formatCurrentTime(boolean millis) {
        Calendar Cld = Calendar.getInstance();
        int yy = Cld.get(Calendar.YEAR);
        int mh = Cld.get(Calendar.MONTH) + 1;
        int dd = Cld.get(Calendar.DATE);
        int hh = Cld.get(Calendar.HOUR_OF_DAY);
        int mm = Cld.get(Calendar.MINUTE);
        int ss = Cld.get(Calendar.SECOND);
        int mi = Cld.get(Calendar.MILLISECOND);
        //DecimalFormat df = new DecimalFormat("#,##0.000");
        DecimalFormat df1 = new DecimalFormat("#,##000");

        return(String.format("%04d-%02d-%02dT%02d:%02d:%02d", yy, mh, dd, hh, mm, ss)+(millis ? ("."+df1.format(mi)) : "")+"+09:00");//millisならミリ秒を足す
    }

    /** format including milliseconds
     **/
    public String formatCurrentMillis() {
        return(formatCurrentTime(true));
    }

    public String writeData(String type, String filePostfix, String values) {
        return writeData(filePostfix, type, values, values);
    }


    /**
     * Save the file and send via UDP
     * @param filePostfix latter part of the filename
     * @param type  sensor type
     * @param values valuses (start with ",")
     * @param udpValues
     * @return filePostfix
     **/
    public String writeData(String type, String filePostfix, String values, String udpValues) {
        if (filePostfix == null)
            filePostfix = formatCurrentTime();

        String timeStr = formatCurrentMillis();

        String message = timeStr + "," + values + "\n";

        //Log.v(TAG,type+ " "+ filePostfix+" "+message);

        //UDP transmit
        try {
            UDP.send(type + "," + udpValues);
        } catch (SocketException e) {
        } catch (Exception e) {
            e.printStackTrace();
        }


        filePostfix = writeTimeFile(type, filePostfix, message);

        return(filePostfix);
    }



    //timestampを付けて保存．timestampが更新されたらそれを返す．
    public String writeTimeFile(String type, String time, String message){
        int max_size = 10000;

        String filename = type + "_" + time + ".csv";

        File file = new File(context.getFilesDir(), filename);
        //Log.v(TAG, "File size: "+ file.length());
        if (file.length() > max_size) {
            //新しくファイル名を設定
            time = formatCurrentTime();
            filename = type + "_" + time + ".csv";
            Log.v(TAG, "New file: "+filename);
        }

        writeFile(filename, message);
        return time;
    }

    public void writeFile(String fileName, String message) {
        try {
            //Log.v(TAG, "writeFile: "+fileName+", "+ message);
            FileOutputStream fout = context.openFileOutput(fileName, Context.MODE_APPEND);
            byte[] bytes = message.getBytes();

            fout.write(bytes);
            fout.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private static String f0;
    //例外をファイルに保存して後でサーバに送付
    public void writeLog(Throwable t) {
        try {

            if (f0 == null) f0 = formatCurrentTime();

            // スタックトレースを文字列に.
            StringWriter stringWriter = new StringWriter();
            t.printStackTrace(new PrintWriter(stringWriter));
            String message = stringWriter.toString();

            Log.v("writeLog", message);
            f0 = writeTimeFile("debug", f0, message);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }




}
