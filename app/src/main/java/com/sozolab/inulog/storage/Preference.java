package com.sozolab.inulog.storage;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.annotation.MainThread;

import com.sozolab.inulog.MyApp;
import com.sozolab.inulog.activity.MainActivity;

/**
 * Shared preference wrapper
 * Created by sozo on 2017/08/08.
 */

public class Preference {
    private static SharedPreferences prefs;


    public static void init(SharedPreferences prefs){
        Preference.prefs = prefs;
    }


    /*public Preference(Context c){
         prefs = c.getSharedPreferences("prefs", 0);//initialize
    }*/

    public static void set(String key, int value){
        SharedPreferences.Editor editor = prefs.edit();
        editor.putInt(key, value);
        editor.commit();
    }

    public static void set(String key, String value){
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(key, value);
        editor.commit();
    }

    public static void set(String key, boolean value){
        SharedPreferences.Editor editor = prefs.edit();
        editor.putBoolean(key, value);
        editor.commit();
    }



    public static void setIfEmpty(String key, String value){
        if (!prefs.contains(key)) set(key, value);
    }

    public static void setIfEmpty(String key, boolean value){
        if (!prefs.contains(key)) set(key, value);
    }


    public static int getInt(String key){
        return prefs.getInt(key, -1);
    }

    public static String get(String key){
        return prefs.getString(key, "");
    }

    public static boolean getBoolean(String key){
        return prefs.getBoolean(key, false);

    }
}
