package com.sozolab.inulog.appControl;


import com.sozolab.inulog.activity.MainActivity;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

public class BootBroadcastReceiver extends BroadcastReceiver {
    static final String action_boot="android.intent.action.BOOT_COMPLETED";//ブート時に起動
    static final String action_replaced="android.intent.action.PACKAGE_REPLACED";//アプリ更新時に起動

    MainActivity activity;//このactivityを常に初期化したいが．

    public void init(MainActivity activity){
        this.activity = activity;
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        Log.d("Broadcast", intent.getAction());
        String action = intent.getAction();
        if (action!=null){
            Log.i("Broadcast", action);
            //起動，本当はactivity.restartにしたいけどNullPointerなので．
            if (action.equals(action_boot)) {

                if (activity!=null) activity.io.writeLog(new Exception("OS_BOOT_COMPLETED"));

                //activity.restart();

                //起動，本当はactivity.restartにしたいけどNullPointerなので．
                Intent startIntent = new Intent(context, MainActivity.class);
                startIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(startIntent);

            } else if (action.equals(action_replaced)) {

                if (activity!=null) {
                    activity.io.writeLog(new Exception("PACKAGE_REPLACED"));
                    //activity.restart();
                }

                //起動，本当はactivity.restartにしたいけどNullPointerなので．
                Intent startIntent = new Intent(context, MainActivity.class);
                startIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(startIntent);

            } else if (action.equals(Intent.ACTION_SCREEN_ON)) {
                Log.i("Status","SCREEN_ON");
                // 画面ON時
                //if(activity.vibrator!=null)activity.vibrator.cancel(); //vibration をoff
            } else if (action.equals(Intent.ACTION_SCREEN_OFF)) {
                Log.i("Status","SCREEN_OFF");
                // 画面OFF時
            }
        }

    }



}

