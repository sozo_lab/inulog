package com.sozolab.inulog.appControl;

/**
 * Created by sozo on 2017/02/20.
 */

//アプリの終了などを検知
import android.app.Application;
import android.util.Log;


public class AppControl extends Application {
    @Override
    public void onCreate() {
        super.onCreate();
        Log.i("Status", "App onCreate");
        // アプリ起動時の処理

    }

    @Override
    public void onTerminate() {
        super.onTerminate();
        Log.i("Status", "App onTerminate");
        // アプリ終了時の処理
    }
}
