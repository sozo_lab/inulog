package com.sozolab.domain;

import android.util.Log;
import android.util.Xml;

import com.sozolab.inulog.storage.Preference;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import java.io.StringReader;
import java.util.ArrayList;

/**
 * Created by sozo on 2017/08/07.
 */

public class ActivityType {

    private static Preference prefs;

    public static final int NULL=0, ID=1, NAME=2, INSTANT=3;

    public int id;
    public String name;
    public boolean instant; // if the activity finishes soon.


    public void set(int valueType, String value){
        if (valueType == ID) id = Integer.valueOf(value);
        else if (valueType == INSTANT) instant = Boolean.valueOf(value);
        else if (valueType == NAME) name = value;
    }


    public static ArrayList<ActivityType> getAll(){
        ArrayList<ActivityType> result = new ArrayList<ActivityType>();

        String xml = prefs.get("ActivityTypes");

        Log.d("ActivityType", xml);

        XmlPullParser parser = Xml.newPullParser();

        try {
            parser.setInput( new StringReader(xml) );
        } catch (XmlPullParserException e) {
            Log.d("parserSample", "Error");
        }

        try {
            int eventType;
            int valueType = ActivityType.NULL;
            ActivityType activityType = new ActivityType();//temporary new
            eventType = parser.getEventType();
            while (eventType != parser.END_DOCUMENT) {
                if(eventType == parser.START_TAG) {
                    //Log.d("ActivityType", "start:"+parser.getName());
                    if (parser.getName().equals("activity-type")) {
                        activityType = new ActivityType();
                    } else if (parser.getName().equals("id")) {
                        valueType = ActivityType.ID;
                    } else if (parser.getName().equals("name")) {
                        valueType = ActivityType.NAME;
                    } else if (parser.getName().equals("instant")) {
                        valueType = ActivityType.INSTANT;
                    }
                } else if(eventType == parser.END_TAG) {
                    valueType = ActivityType.NULL;

                    //Log.d("ActivityType", "end:"+ parser.getName());

                    if (parser.getName().equals("activity-type")) {
                        Log.d("ActivityType", "name:"+activityType.name + " id:"+ activityType.id + " instant:"+activityType.instant);
                        result.add(activityType);
                    }
                } else if(eventType == parser.TEXT) {
                    //Log.d("ActivityType", "text:"+parser.getText());
                    if (valueType!=ActivityType.NULL) activityType.set(valueType, parser.getText());

                }
                eventType = parser.next();
            }
        } catch (Exception e) {
            Log.d("parserSample", "Error");
        }
        return result;
    }



}
